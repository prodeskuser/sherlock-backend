$(document).ready(function(){

    // Side bar toggle menu
    $(".left-arrow-link").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    // Menu widget
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $(".full-width-menu").toggleClass("toggled");
    });

    // Add plus minus icons on collapsing and expanding sidebar
    $(".collapse.in").each(function(){
        $(this).siblings(".panel-heading").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });
    $(".collapse").on('show.bs.collapse', function(){
        $(this).parent().find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function(){
        $(this).parent().find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});