$(function() {
    //Set current date
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    }
    if(mm<10) {
        mm = '0'+mm
    } 
    today = yyyy + '/' + mm + '/' + dd;

    var makeDate = new Date(mm + '/' + dd + '/' + yyyy);
    makeDate = new Date(makeDate.setMonth(makeDate.getMonth() - 1));
    //Set last date
    var dd = makeDate.getDate();
    var mm = makeDate.getMonth()+1; //January is 0!
    var yyyy = makeDate.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    } 
    if(mm<10) {
        mm = '0'+mm
    }
    makeDate = yyyy + '/' + mm + '/' + dd;

    $("#date_range").daterangepicker({
        autoUpdateInput: true,
        opens: 'left',
        "drops": "down",
        startDate: makeDate,
        endDate: today,
        locale: {
            format: 'YYYY/MM/DD',
            cancelLabel: 'Clear'
        },
        ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Years': [moment().startOf('years'), moment().endOf('years')]
        }
    }, function(start, end, label) {
        $('#date_range').val(start.format('DD/MM/YYYY')+'-'+end.format('DD/MM/YYYY'));
    });
});