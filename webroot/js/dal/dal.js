/*
 * data access layer
 */
var Dal = function(){
    /*
     * Public method
     * Can be called outside class
     */
    this.getReport  = function(platform=false, account_id, reportType=false, date_range=false, attribution_window=false , callback_function, instance){
        var options = {};

        if(platform){
            options.platform = platform;
        }
        options.account_id = account_id;
        options.report_type = reportType;
        if(date_range){
            options.to      = date_range[1];
            options.from    = date_range[0];
        }
        if(attribution_window){
            options.attribution_window = attribution_window;
        }
        $.ajax({
            type      :  'GET',
            dataType  :  'json',
            url       :  "/ApiReports/get/"+platform+"/"+account_id+"/"+reportType,
            beforeSend: function(){
            },
            async     :  false,
            data      :  options,
            success   :  function(data) {
                callback_function(data,instance);
            }
        }); 
    };

    this.saveReportView  = function(account_id, columns, view_name){
        $.ajax({
            type      :  'POST',
            dataType  :  'json',
            url       :  "/ReportViews/addview",
            beforeSend: function(){
            },
            async     :  false,
            data      :  {"account_id":account_id, "columns":columns, 'view_name':view_name},
            success   :  function(data) {
                return data;
            },
            error: function() {
            },
        }); 
    }

    this.manageconfigColumns  = function(method, action, data="", handleData, instance=false){
         return $.ajax({
            type      :  method,
            dataType  :  'json',
            url       :  "/UserColumns/"+action,
            async     :  false,
            data      :  {"data":data},
            success   :  function(r) {
                handleData(r,instance);
            }
        }); 
    }
};