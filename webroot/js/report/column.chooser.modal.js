var ColumnChooserModal = function(){

    this.InitModel = function(table_element, input) {
        var datagrid = table_element.dxDataGrid("instance");
        var columns = datagrid.state();
        this.resetModal();
        $('.columnContainer').empty();
        $('#sortable').empty();
        $.each(columns.columns, function(key, val) {

            //Checked Visible Column
            var checkbox_condition = ((val.visible) ? 'Checked' : '');
            $('.columnContainer').append('<li class="form-check"><input id="'+val.visibleIndex+'" type="checkbox" value="'+btoa(val.dataField)+'" class="form-check-input available_columns" '+checkbox_condition+' />'+val.name+'</li>');

            if(val.visible){
                $('#sortable').append('<li class="ui-state-default" id="srt_'+btoa(val.dataField)+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+val.dataField+'<a href="#" class="sortremove ui-icon ui-icon-closethick pull-right" id="'+btoa(val.name)+'">remove</a></li>');
            }
        });

        var selected_columns = [];
        $('.form-check-input').on('change', function (e) {
            datagrid.beginUpdate();
            for (var i = 0; i < columns.columns.length; i++) {
                //Visible Indexing
                if (columns.columns[i].dataField === atob(this.value)) {
                    if($(this).is(':checked')){
                        datagrid.columnOption(columns.columns[i].dataField, "visible", true);
                        break;
                    }else{
                        datagrid.columnOption(columns.columns[i].dataField, "visible", false);
                        break;
                    }
                }
            }
            datagrid.endUpdate();
            selected_columns = $(".form-check-input:checked").map(function(){
                return $(this).val();
            }).get();

            $('#sortable').empty();
            $.each(selected_columns, function(i, column) {
                $('#sortable').append('<li class="ui-state-default" id="srt_'+column+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+atob(column)+'<a href="#" class="sortremove ui-icon ui-icon-closethick pull-right" id="'+column+'">remove</a></li>');
            });

            $(".sortremove").click(function(e) {
                $(this).parent().remove();
                $("input[type=checkbox][value='"+$(this).attr('id')+"']").prop("checked",false);
                datagrid.columnOption(atob($(this).attr('id')), "visible", false);
            });
        });

        $(".sortremove").click(function(e) {
            $(this).parent().remove();
            $("input[type=checkbox][value='"+$(this).attr('id')+"']").prop("checked",false);
            datagrid.columnOption(atob($(this).attr('id')), "visible", false);
        });

        $('#sortable').sortable({
            forcePlaceholderSize: true,
            tolerance: 'move',
            cursor: 'move',
            over: function () { removeIntent = true; },
            out: function () { removeIntent = true; },
            update: function(event, ui) {
                var element_id = ui.item.attr('id');
                table_element.dxDataGrid("columnOption", atob(element_id.substring(4)), "visibleIndex", ui.item.index());
            },
            start: function(event, ui) {},
            remove: function( event, ui ) {},
            revert: true
        });
        $('#sortable').disableSelection();

        $(".save_view_type").hide();
        $('.save_preset').on('change', function (e) {
            if($(this).is(':checked')){
                $(".save_view_type").show();
            }else{
                $(".save_view_type").hide();
            }
        });
    }

    this.SearchColumn = function() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchColumn");
        filter = input.value.toUpperCase();
        table = document.getElementById("searchContainer");
        tr = table.getElementsByClassName("available_columns");
        for (i = 0; i < tr.length; i++) {
            var td = $(tr[i]).parent().text();
            if (td) {
                if (td.toUpperCase().indexOf(filter) > -1) {
                    $(tr[i]).parent().css("display","");
                } else {
                    $(tr[i]).parent().css("display","none");
                }
            }
        }
    }

    //ResetModalInput
    this.resetModal = function(){
        $('#view').val('');
        $('#searchColumn').val('');
        $('.invalid-feedback').hide();
        $(".save_preset").prop("checked",false);
    }

    //Update User VIews SelectBox
    this.updateSelectBox = function(columnsdata) {
        $('#ColumnChooserModal').modal('toggle');
        $('#column_selector').empty();
        $.each(columnsdata.user_views, function(key, view) {
            $('#column_selector').append($('<option/>', {
               value: view,
               text : view
            }));
        });
    }

    this.saveColumn = function(table_div, input){
        $('.invalid-feedback').hide();
        var viewName = $('#view').val();
        //Check spaces.
        if(document.getElementById("view").value.replace(/\s/g, '') == ""){
            document.getElementById("view").focus();
            dataGridObj.hideLoadPanel();
            return false;
        }

        //Check alreday exist.
        var user_views = dataGridObj.getUserView();
        if(user_views[viewName]){
            $('#view').css({'border-color':'#dc3545'});
            $('.invalid-feedback').show();
            dataGridObj.hideLoadPanel();
            return false;
        }

        //Save view.
        $('#view').css({'border-color':'#dee2e6'});
        var gridState = table_div.dxDataGrid("instance").state();
        var dalObj = new Dal();
        dalObj.saveReportView($('#account_id').val(), JSON.stringify(gridState), viewName);

        //Get view.
        dataGridObj.getConfigData();

        //Apply view into filters
        dataGridObj.getUserView();
        var accountData = dataGridObj.getAccountdatails();
        this.updateSelectBox(accountData[$('#account_id').val()])
        $('#column_selector  option[value="'+viewName+'"]').prop("selected", true);
        dataGridObj.hideLoadPanel();
    }
}
