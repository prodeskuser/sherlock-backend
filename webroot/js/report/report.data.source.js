var ReportDataSource = function(reportJsonData, columnsConfigData){

    this.result = reportJsonData;
    this.reportData = [];
    this.columns = [];

    this.BuildCustomColumns = function(report_view_columns) {

        var columns_config_arr = [];
        var columns = [];
        var reportData = this.result.msgs;
        var view_columns = [];
        $.each(report_view_columns.columns, function(key, val) {
            view_columns[val.dataField] = val;
        });

        $.each(columnsConfigData, function(key, columns_config) {
            $.each(reportData, function(key2, val2) {
                var expression = columns_config.expression;
                $.each(val2, function(key3, val3) {
                    if(columns_config.type == 'Custom'){
                        if(expression.indexOf(key3) != -1){
                            expression = expression.replace(key3, val3);
                        }
                    }
                    else {
                        if(key3 == columns_config.db_name && typeof val3 === 'number'){
                            reportData[key2][key3] = val3.toFixed(columns_config.decimal_digits);
                        }
                    }
                });
                if(columns_config.type == 'Custom'){
                    reportData[key2][columns_config.db_name] = eval(expression).toFixed(columns_config.decimal_digits);
                }
            });

            columns.push({
                dataField:columns_config.db_name,
                caption:columns_config.user_name,
                column:columns_config.user_name,
                custom_column:((columns_config.type == 'Core') ? columns_config.db_name+'_core' : columns_config.user_name+'_custom'),
                displayFormat:'{0}',
                alignment: (view_columns[columns_config.db_name] && view_columns[columns_config.db_name].dataType == "date") ? 'left' : 'right', /* Date field applied */
                width: 'auto',
                dataType: (columns_config.db_name == "request_date") ? "date" : "",
                showInColumn:columns_config.user_name,
                format: 'dd-MM-yyyy',
                summaryType:((columns_config.type == 'Core' && (!(view_columns[columns_config.db_name]) || (view_columns[columns_config.db_name].dataType != "date"))) ? 'sum' : 'custom'),
                valueFormat: ((columns_config.type == 'Core' && (!(view_columns[columns_config.db_name]) || (view_columns[columns_config.db_name].dataType != "date"))) ? { type: "fixedPoint", precision: columns_config.decimal_digits} : ' '),
                name:columns_config.user_name,
                expression:columns_config.expression,
                visibleIndex: (view_columns[columns_config.db_name]) ? view_columns[columns_config.db_name].visibleIndex : false,
                visible: (view_columns[columns_config.db_name]) ? view_columns[columns_config.db_name].visible : false
            });
        });
        this.reportData = reportData;
        this.columns = columns;
    }

    this.getData = function() {
        return this.reportData;
    }

    this.getColumns = function() {
        return this.columns;
    }
};