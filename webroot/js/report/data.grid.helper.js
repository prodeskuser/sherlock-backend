var DataGrid = function(){
var dalObj = new Dal();
    this.getConfigData = function(){

        dalObj.manageconfigColumns("get", "getReportConfigData","", 
            function(response, instance){
                instance.user_views = response.user_views;
                instance.columns_config_data = response.columns_config_data;
                instance.account_data = response.account_data;
            },this
        );
    }

    this.Initialize = function(tbl_element, input) {

        this.report_view = input["report_view"].val();
        this.account_id = input["account_id"].val();
        this.report_type = (input["report_type"].val()) ? input["report_type"].val() : 'by_date';
        this.date_range = input["date_range"].val().split("-");
        this.attribution_window = (input["attribution_window"]) ? input["attribution_window"] : 'default';
        this.platform = input["platform"] ? input["platform"] : null;

        dalObj.getReport(
            this.platform, this.account_id, this.report_type, this.date_range, this.attribution_window, 
            function(response, instance){
                instance.response = response;
            },this
        );

        var report_view_columns = this.user_views[this.report_view];
        report_view_columns = $.parseJSON(report_view_columns);
        
        // Customise input data and build custom columns
        var reportDataSourceObj = new ReportDataSource(this.response, this.columns_config_data);
        reportDataSourceObj.BuildCustomColumns(report_view_columns);
        
        // Prepare datasource and initiate data grid
        var dataSource = this.prepareGridData(reportDataSourceObj.getData());
        this.initiateGrid(tbl_element, reportDataSourceObj.getColumns(), dataSource, this.selected_view_columns, this.columns_config_data);
    }
    
    this.prepareGridData = function(data){
        var dataSource = new DevExpress.data.CustomStore({
            load: function(loadOptions) {
                var deferred = $.Deferred(),
                    args = {};

                if(loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if(loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }
                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 12;
                if(data){
                    deferred.resolve(data, { totalCount: 100 });
                }else{
                    deferred.reject("Data Loading Error");
                }
                return deferred.promise();
            }
        });
        return dataSource;
    }

    this.initiateGrid = function(tbl_element, columns, dataSource, selected_view_columns, columns_config_data){
        this.gridObj = tbl_element.dxDataGrid({
            selection: {
                mode: "multiple"
            },
            dataSource: {
                store: dataSource
            },
            loadPanel: {
                enabled: true
            },
            pager: {
                enabled : false
            },
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            scrolling: {
                mode: "infinite"
            },
            allowColumnResizing: true,
            columnFixing: {
                enabled: true
            },
            headerFilter: {
                visible: true
            },
            columnsAutoWidth: true,
            onExporting: function(e) {
                loadPanel.show();
                },
            onExported: function (e) {
                loadPanel.hide();
            },
            "export": {
                enabled: false,
                fileName: "reports",
                allowExportSelectedData: true
            },
            columnFixing: {
                enabled: true
            },
            allowColumnResizing: true,
            columnResizingMode: "nextColumn",
            allowColumnReordering: true,
            showColumnLines: true,
            showRowLines: true,
            rowAlternationEnabled: true,
            showBorders: true,
            stateStoring: {
                enabled: true,
                type: "custom",
                storageKey: "storage",
                customLoad: function () {},
                customSave: function (state) {}
            },
            onCellPrepared: function (e) {
                e.element.find(".dx-datagrid-total-footer")
                .css("background-color", "#ebeff1")
                .insertBefore(e.element.find(".dx-datagrid-rowsview"));
                if (e.rowType == "totalFooter") {
                    // Calculating CustomColumnSummary by evaluating CoreColumnsummary
                    let si_custom = e.summaryItems.find(i => i.custom_column.endsWith("_custom"));
                    if (si_custom) {
                        var expression = si_custom.expression;
                        $.each(columns_config_data, function(key, val) {
                            if(expression.indexOf(val.db_name) != -1){
                                var coreColumnValue = e.component.getTotalSummaryValue(val.user_name);
                                expression = expression.replace(val.db_name, coreColumnValue);
                            }
                        });
                        e.cellElement[0].innerText = eval(expression).toFixed(si_custom.decimal_digits);
                    }

                    // Make the decimal digits dynamic for core column summaries
                    let si_core = e.summaryItems.find(i => i.custom_column.endsWith("_core"));
                    if (si_core) {
                        var innerTextVal = e.cellElement[0].innerText;
                        if(typeof innerTextVal == 'number') {
                            e.cellElement[0].innerText = innerTextVal.toFixed(si_core.decimal_digits);
                        }
                    }
                }
            },
            summary: {
                    totalItems: columns,
                    calculateCustomSummary: function(options) {
                        if(options.summaryProcess == 'start'){ options.totalValue = 0; }
                        if(options.summaryProcess == 'calculate'){}
                        if(options.summaryProcess == 'finalize'){ options.totalValue = options.totalValue; }
                    }
            },
            columns: columns
        }).dxDataGrid("instance");
    }
    

    this.changeView = function(table_view, tbl_element){
        var grid = this.gridObj;
        var state = {};
        if(table_view == "default") {
            tbl_element.dxDataGrid("instance").state({});
        } else {
            state = this.user_views[table_view];
            setTimeout(function(){
                state = $.parseJSON(state);
                grid.beginUpdate();
                for (var i = 0; i < state.columns.length; i++) {
                    grid.columnOption(state.columns[i].dataField, "visible", state.columns[i].visible);
                    grid.columnOption(state.columns[i].dataField, "visibleIndex", state.columns[i].visibleIndex);
                }
                grid.endUpdate();
            }, 10);
        }
    }

    this.exportGridData = function(tbl_element){
        tbl_element.dxDataGrid("instance").exportToExcel(false);
    }

    this.getAccountdatails = function(){
        return this.account_data;
    }

    this.getUserView = function(){
        return this.user_views;
    }

    this.showLoadPanel = function() {
        loadPanel.show();
    };

    this.hideLoadPanel = function() {
        loadPanel.hide();
    };
}