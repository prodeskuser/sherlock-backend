// Initiating default loader
var loadPanel = $("#loadPanelContainer").dxLoadPanel({
    shadingColor: "rgba(0,0,0,0.4)",
    visible: true,
    showIndicator: true,
    showPane: true,
    shading: true,
    closeOnOutsideClick: false
}).dxLoadPanel("instance");

var dataGridObj = new DataGrid();
var ColumnChooserModalObj = new ColumnChooserModal();
var table_div = $("#DimensionsTable");
var input = [];

var acc_selbox_el = $('#account_id');
var attr_selbox_el = $('#attribution_window');
var column_selector_el = $('#column_selector');


$(document).ready(function(){
    // Load config data and initiate Report Grid on PageLoad
    dataGridObj.getConfigData();

    var accountData = dataGridObj.getAccountdatails();
    SetReportFilter(accountData);
    $("#date_range").change(function(){
        initiateReportGrid();
    });
    dataGridObj.hideLoadPanel();
});

function initiateReportGrid() {
    // Initiate the report grid

    input["report_view"] = $("#column_selector option:selected");
    input["account_id"] = acc_selbox_el;
    input["report_type"] = $('#report_type');
    input["date_range"] = $('#date_range');
    input["attribution_window"] = $('#attribution_window').val();
    input["platform"] = null;
    setTimeout(function(){
        dataGridObj.Initialize(table_div, input);
    }, 0);
}

// Change report view
$('#column_selector').on('change', function (e) {
    dataGridObj.changeView(this.value, table_div);
});

// Export report
$("#export").click(function(){
    dataGridObj.exportGridData(table_div);
});

$("#btnSave").dxButton({
    text: "Save Changes",
    onClick: function () {
        dataGridObj.showLoadPanel();
        setTimeout(function(){
            ColumnChooserModalObj.saveColumn(table_div, input);
        }, 0);
    }
});

function initiateColumnChooserModal(){
    ColumnChooserModalObj.InitModel(table_div);
}

function searchColumn(){
    ColumnChooserModalObj.SearchColumn();
}

function applyFilter(selectedAccountData){
    attr_selbox_el.empty();
    column_selector_el.empty();
    $.each(selectedAccountData.attribution_window, function(key, attributions) {
        attr_selbox_el.append($('<option/>', { 
            value: attributions,
            text : attributions 
        }));
    });

    $.each(selectedAccountData.user_views, function(key, views) {
        column_selector_el.append($('<option/>', { 
            value: views,
            text : views 
        }));
    });
    initiateReportGrid();
}

acc_selbox_el.change(function(){
    var accountData = dataGridObj.getAccountdatails();
    selectedAccountData = accountData[$(this).val()];
    applyFilter(selectedAccountData);
});

function SetReportFilter(accountData){
    acc_selbox_el.empty();
    attr_selbox_el.empty();
    column_selector_el.empty();
    var count = 0;
    $.each(accountData, function(key, item) {
        count++;
        acc_selbox_el.append($('<option/>', {
            value: item.account_id,text : item.account_name 
        }));

        if ( count === 1) {
            $.each(item.attribution_window, function(key, attributions) {
                attr_selbox_el.append($('<option/>', { 
                    value: attributions,
                    text : attributions 
                }));
            });

            $.each(item.user_views, function(key, views) {
                column_selector_el.append($('<option/>', { 
                    value: views,
                    text : views 
                }));
            });
        }
    });
    initiateReportGrid();
}