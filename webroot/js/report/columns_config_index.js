$(function() { 
    var dalClass = new Dal();
    var platform = [{
        "ID": 'Facebook',
        "Name": "Facebook"
    }];

    var type = [{
        "ID": 'Core',
        "Name": "Core"
    }, {
        "ID": 'Custom',
        "Name": "Custom"
    }];

    var columns_config =  new DevExpress.data.CustomStore({
        load: function(loadOptions) {
            var deferred = $.Deferred(),
                args = {};
            if(loadOptions.sort) {
                args.orderby = loadOptions.sort[0].selector;
                if(loadOptions.sort[0].desc)
                    args.orderby += " desc";
            }
            args.skip = loadOptions.skip || 0;
            args.take = loadOptions.take || 12;

            dalClass.manageconfigColumns('GET', 'getUserColumnsConfigData', null, function(output){ var result = output; 
                deferred.resolve(result.result, { totalCount: result.totalCount });
            });
            return deferred.promise();
        },
        byKey: function(key) {
            return dalClass.manageconfigColumns('GET', 'getUserColumnsConfigData', null, function(output){ return output; });
        },
        insert: function(values) {
            return dalClass.manageconfigColumns('POST', 'saveColumn', values, function(output){ return output; });
        },
        update: function(key, values, extra) {
            var data = {'oldData':key,'newData':values};
            return dalClass.manageconfigColumns('POST', 'updateColumn', data, function(output){ return output; });
        },
        remove: function(key, values, extra) {
            return dalClass.manageconfigColumns('POST', 'removeColumn', key, function(output){ return output; });
        }
    });

    $("#grid").dxDataGrid({
        dataSource: {
            store: columns_config
        },
        keyExpr: "ID",
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true
        },
        remoteOperations: {
            sorting: true,
        },
        scrolling: {
            mode: "virtual"
        },
        editing: {
            mode: 'row',
            allowUpdating: true,
            allowAdding: true,
            allowDeleting: true,
            removeEnabled: true
        },
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        showBorders: true,
        onCellPrepared: function(cellElement, cellInfo) {
            if(cellElement.rowType == "data" && cellElement.data.type == "Core")
                cellElement.cellElement.find(".dx-link-delete").remove();
            if(cellElement.rowType != "data" || !cellElement.isEditing)
                return;
            if((cellElement.column.dataField === 'id' && !cellElement.row.inserted)  || (cellElement.column.dataField === 'decimal_digits' && !cellElement.row.inserted)|| (cellElement.column.dataField === 'user_id' && !cellElement.row.inserted) || (cellElement.column.dataField === 'platform' && !cellElement.row.inserted) || (cellElement.column.dataField === 'type' && !cellElement.row.inserted) || (cellElement.column.dataField === 'db_name' && !cellElement.row.inserted))
                cellElement.element.find('input').prop('readonly', true);
        },
        columns: [
            {dataField: 'platform', width: '100', allowEditing : true, lookup: {
                    dataSource: platform,
                    displayExpr: "Name",
                    valueExpr: "ID"
                }},
            {dataField: 'type', width: '100', allowEditing : true, lookup: {
                    dataSource: type,
                    displayExpr: "Name",
                    valueExpr: "ID"
                }},
            {dataField: 'db_name', allowEditing: true},
            {dataField: 'user_name', allowEditing: true},
            {dataField: 'expression', allowEditing : true},
            {dataField: 'decimal_digits', allowEditing:true, lookup: { dataSource: [0,1,2,3,4]}},
        ],
    }).dxDataGrid("instance");
});