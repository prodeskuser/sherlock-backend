 $(function() {
    var columns_config = new DevExpress.data.CustomStore({
        load: function(loadOptions) {
            var deferred = $.Deferred(),
                args = {};

            if(loadOptions.sort) {
                args.orderby = loadOptions.sort[0].selector;
                if(loadOptions.sort[0].desc)
                    args.orderby += " desc";
            }

            args.skip = loadOptions.skip || 0;
            args.take = loadOptions.take || 12;

            $.ajax({
                url: "/UserColumns/getUserColumnsConfigData",
                type: 'POST',
                dataType: "json",
                data: args,
                success: function(result) {
                    deferred.resolve(result.result, { totalCount: result.totalCount });
                },
                error: function() {
                    deferred.reject("Data Loading Error");
                },
                timeout: 5000
            });

            return deferred.promise();
        }
    });

    $("#gridContainer").dxDataGrid({
        dataSource: {
            store: columns_config
        },
        keyExpr: "ID",
        filterRow: {
            visible: true,
            applyFilter: "auto"
        },
        headerFilter: {
            visible: true
        },
        remoteOperations: {
            sorting: true,
            //paging: false
        },
        scrolling: {
            mode: "virtual"
        },
        editing: {
            editMode: 'batch',
            allowUpdating: true,
            allowAdding: true
        },
        showColumnLines: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        showBorders: true,
        onCellPrepared: function(cellElement, cellInfo) {
            if(cellElement.rowType != "data" || !cellElement.isEditing)
                return;
            if((cellElement.column.dataField === 'id' && !cellElement.row.inserted) || (cellElement.column.dataField === 'user_id' && !cellElement.row.inserted) || (cellElement.column.dataField === 'platform' && !cellElement.row.inserted) || (cellElement.column.dataField === 'type' && !cellElement.row.inserted) || (cellElement.column.dataField === 'db_name' && !cellElement.row.inserted))
                cellElement.element.find('input').prop('readonly', true);
        },
        onRowInserting: function(e) {
            //console.log(e);

            $.ajax({
                url: "/UserColumns/saveColumn",
                type: 'POST',
                data: {'data' : e.data},
                //dataType: "json",
                success: function(res) {
                    if(res.msg != '' && res.msg == "success") {
                        alert('Saved successfully');
                    } else {
                        
                    }
                },
                error: function() {
                    
                },
                timeout: 5000
            });
        },
        onRowInserted: function(info) {
            //alert('row insterted');
        },
        onRowUpdating: function(e) {
            //console.log(e);

            var user_name = e.newData.user_name;
            var col_id = e.oldData.id;

            $.ajax({
                url: "/UserColumns/updateColumn",
                type: 'POST',
                data: {'newData' : e.newData, 'oldData' : e.oldData},
                dataType: "json",
                success: function(res) {
                    if(res.msg != '' && res.msg == "success") {
                        var dataGrid = $('#gridContainer').dxDataGrid('instance');
                        dataGrid.refresh();
                    } else {
                        
                    }
                },
                error: function() {
                    
                },
                timeout: 5000
            });
        },
        columns: [
            {dataField: 'platform', width: '100', allowEditing : true, lookup: {
                    dataSource: platform,
                    displayExpr: "Name",
                    valueExpr: "ID"
                }},
            {dataField: 'type', width: '100', allowEditing : true, lookup: {
                    dataSource: type,
                    displayExpr: "Name",
                    valueExpr: "ID"
                }},
            {dataField: 'db_name', allowEditing: true},
            {dataField: 'user_name', allowEditing: true},
            {dataField: 'expression', allowEditing : true},
        ],
    }).dxDataGrid("instance");
});