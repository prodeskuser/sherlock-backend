<?php

namespace App\Shell;
use Exception;
use App\Model\Table\FacebookAdInsightsActionsTable;
use App\Model\Table\FacebookCustomConversionsTable;
use App\Model\Table\FacebookAdInsightsTable;

use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdsInsightsFields;
use FacebookAds\Object\Values\AdsInsightsLevelValues;
use FacebookAds\Cursor;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class FBInsightsShell extends Shell {

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('FacebookAdAccounts');
        $this->loadModel('FacebookAdInsights');
        $this->loadModel('FacebookAdInsightsActions');
        $this->loadModel('FacebookTokens');
        date_default_timezone_set('Etc/GMT-2');
    }

    public function DownloadInsightsShell($days=1, $shift_back_days=0, $account_id_param=null)
    {
        $range = $this->time_range($days, $shift_back_days);
        $db_tokens = $this->FacebookTokens->getAllTokens();
        $report_runs = array();

        $downloaded_accounts=[];

        foreach ($db_tokens as $tok) {  
            $this->init_api($tok->access_token);
            Cursor::setDefaultUseImplicitFetch(true);

            $NUM_OF_ATTEMPTS = 3;
            $attempts = 0;
            do {
                try
                {
                    $report_runs[$tok->user_id] = $this->start_async_jobs($tok->user_id, $range,$downloaded_accounts);
                } catch (Exception $e) {
                    $attempts++;
                    continue;
                }
                break;
            } while($attempts < $NUM_OF_ATTEMPTS);
        }
        $this->poll_fetch($report_runs);
    }


    // ---------------------------------
    // Private methods
    // ---------------------------------
    private function start_async_jobs($user_id, $range,&$downloaded_accounts=[])
    { 
        $report_runs    = array();
        $fields         = FacebookAdInsightsTable::getInsightsFields();
        $db_accounts    = $this->FacebookAdAccounts->getAdAccountsForDownload($user_id);
        $days_range     = $this->getRange($range['since'], $range['until'], 1);

        //$downloaded_accounts=[];
        foreach ($db_accounts as $db_account) { 
            if(isset($downloaded_accounts[$db_account->account_id])) continue;
            foreach ($days_range as $day)
            {
                $day = ['since'=>$day['from'],'until'=>$day['to']];
                $this->out("start_async_jobs...account- {$db_account->account_id} day:" . $day['since']);
                $params = $this->build_insight_params($day,$db_account->attribution_window, $db_account->campaign_filter_string);
                $ad_account = new AdAccount("act_{$db_account->account_id}");
                $report_run = $ad_account->getInsightsAsync($fields, $params);
                $this->out("Report run  {$report_run->id}");
                $report_runs[$db_account->account_id][] = $report_run;
            }
            $downloaded_accounts[$db_account->account_id] = true;
        }
        return $report_runs;
    }

    private function poll_fetch($user_report_runs)
    {
        $timeout = time() + 60 * 5;
        $seconds = 3;
        $all_rows=[];
        $all_rows_actions=[];
        // Poll until results come in
        while (time() < $timeout and !empty($user_report_runs)) {
            $this->out("polling...");
            sleep($seconds);
            foreach ($user_report_runs as $user_id => $account_report_runs) {
                foreach ($account_report_runs as $account_id => $report_runs) {
                    for ($x=0;$x<count($report_runs);$x++) {

                        // ask facebook API for new state
                        if(isset($report_runs[$x])) {
                            $report_run = $report_runs[$x];
                            $report_run = $report_run->getSelf();

                            if ($report_run->isComplete()) {
                                // DOWNLOAD INSIGHTS
                                $insights = $report_run->getInsights();
                                $count = $insights->count();
                                $this->out("Downloading {$count} insights for account {$account_id}.");
                                foreach ($insights as $row) {
                                    $insights_data = $row->getData();
                                    $new_row = $this->flattenInsightsRow ($insights_data);
                                    $row_actions = $this->extractActions($new_row);

                                    $all_rows_actions = array_merge($all_rows_actions,$row_actions);
                                    $all_rows[]=$new_row;
                                }
                                // remove from array
                                unset($user_report_runs[$user_id][$account_id][$x]);
                                if (empty($user_report_runs[$user_id][$account_id])) {
                                    unset($user_report_runs[$user_id][$account_id]);
                                }

                                if (empty($user_report_runs[$user_id])) {
                                    unset($user_report_runs[$user_id]);
                                }
                            }
                        }
                    }
                }
            }
            $this->FacebookAdInsights->saveInsights ($all_rows);
            $this->FacebookAdInsightsActions->saveActions ($all_rows_actions);
            $all_rows=[];$all_rows_actions=[];
        }
    }

    private function init_api($access_token)
    {
        $app_id = Configure::read('Facebook.app_id');
        $app_secret = Configure::read('Facebook.app_secret');
        Api::init($app_id, $app_secret, $access_token);
    }

    private function time_range($days, $shift_back_days=0)
    {
        $since = date('Y-m-d', microtime(true) - ($days+$shift_back_days-1)*24*60*60);
        $until = date('Y-m-d', microtime(true) - $shift_back_days*24*60*60);
        return array(
            'since' => $since,
            'until' => $until
        );
    }

    private function build_insight_params($time_range, $attr_window=["default"], $campaign_filter_string=null)
    {
        $params = array(
            'time_increment' => 1,
            'level' =>  AdsInsightsLevelValues::AD,
            'time_range' => $time_range,
            'filtering' => array(
                array(
                    'field' => 'campaign.effective_status',
                    'operator' => 'IN',
                    'value' => ['ACTIVE', 'PAUSED']
                )
            ),
            'action_attribution_windows'=>$attr_window
        );

        if ($campaign_filter_string) {
            $params['filtering'][] = array(
                'field' => 'campaign.name',
                'operator' => 'CONTAIN',
                'value' => $campaign_filter_string
            );
        }
        return $params;
    }

    private function flattenInsightsRow($row)
    {
        //set mysql dates for date records
        $row['request_date_str'] = $row['date_stop'];
        $row['request_date'] = new Time($row['date_start']);
        $row['date_stop'] = new Time($row['date_stop']);
        $row['date_start'] = new Time($row['date_start']);

        //extract relevance score
        if (isset($row['relevance_score']['score'])){
            $row['relevance_score'] = $row['relevance_score']['score'];
        } else{
            unset ($row['relevance_score']);
        }

        //flatten video actions
        foreach ($row as $key=>$val)
        {
            if ((strpos($key, 'video_') !== false) && is_array ($row[$key])) {
                $row[$key] = $row[$key][0]['value'];
            }
        }
        return $row;
    }

    private function extractActions ($row, $keys=['actions'=>"",'action_values'=>'_value'])
    {
        $extracted=[];
        foreach ($keys as $key=>$postfix)
        {
            if (!isset ($row[$key])) continue;
            $actions = $row[$key];
            foreach ($actions as $action)
            {
                $name = $action['action_type'];
                $fixed_name = FacebookAdInsightsActionsTable::getDBColName($name,$postfix);

                $extracted['default'][$fixed_name]=$action['value'];

                //map custom actions into cusotm fields
                $custom_coversion_name = FacebookCustomConversionsTable::getCustomCoversionDBColName($row['account_id'], $name);

                if($custom_coversion_name != '')
                    $extracted['default'][$custom_coversion_name]=$action['value'];

                foreach ($action as $key=>$val)
                {
                    if ($key=="value" || $key =="action_type")
                        continue;
                    $extracted[$key][$fixed_name] = $val;

                    if($custom_coversion_name != '')
                        $extracted[$key][$custom_coversion_name] = $val;
                }
            }
        }

        //create new rows per attribution window
        $rows=[];
        foreach ($extracted as $attr_window=>$actions)
        {
            $new_row =$row;
            $new_row['attribution_window']=$attr_window;
            foreach ($actions as $name=>$val)
            {
                $new_row[$name]=$val;
            }
            $rows[]=$new_row;
        }
        return $rows;
    }
    
    public static function getRange ($from,$to,$interval=7)
    {
        //$download_date_list = array (date('Y-m-d H:i:s', $start));
        $start = strtotime($from);
        $end = strtotime($to);
        //if range is smaller than interval
        if ($end-$start<$interval*24*60*60)
        {
            return array (array (
                'from'=>$from,'to'=>$to
            ));
        }
        $cur=$start;
        $ranges = array ();
        while ($cur <= $end)
        {
            $end_range = $cur + ($interval-1)*24*60*60;

            if ($end_range>$end) $end_range=$end;
            $ranges[]=array (
                'from'=>date('Y-m-d', $cur),
                'to'=>date('Y-m-d', $end_range),
            );
            $cur+=$interval*24*60*60;
        }
        return $ranges;
    }
}
