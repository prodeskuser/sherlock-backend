<?php

namespace App\Shell;
use Exception;
use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\AdAccountUser;
use FacebookAds\Object\Fields\AdAccountUserFields;
use FacebookAds\Object\CustomConversion;
use FacebookAds\Cursor;
use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class FBConfigurationShell extends Shell {

    //todo future - download account data once, fix structure, generalize insertUpdate in Base Model / Trait


    public function initialize()
    {
        parent::initialize();
        $this->loadModel('FacebookCampaigns');
        $this->loadModel('FacebookAdAccounts');
        $this->loadModel('FacebookCustomConversions');
        $this->loadModel('FacebookAdsets');
        $this->loadModel('FacebookAds');
        $this->loadModel('FacebookTokens');
        
        date_default_timezone_set('Etc/GMT-2');
    }

    private function init_api($access_token)
    {
        $app_id = Configure::read('Facebook.app_id');
        $app_secret = Configure::read('Facebook.app_secret');
        Api::init($app_id, $app_secret, $access_token);
    }

    public function DownloadConfigurationDataShell() {
        $last_update = date("Y-m-d H:i:s");
        // 2018-07-06 10:00:00
        //$last_update = date('Y-m-d', microtime(true));

        $db_tokens = $this->FacebookTokens->getAllTokens();
        foreach($db_tokens as $tok) {
            $this->synchronizeAccount($tok, $last_update);
        }
    }
    
    public function synchronizeAccount($userdata, $last_update) {
        //initialization account
        $this->init_api($userdata->access_token);

        Cursor::setDefaultUseImplicitFetch(true);
        $user = new AdAccountUser($userdata->user->organization_id);
        $user->read(array(AdAccountUserFields::ID));
        $AdAccountFields = $this->FacebookAdAccounts->getAdAccountFields();
        $accounts = $user->getAdAccounts($AdAccountFields);
        $accounts->setUseImplicitFetch(true);

       $count=1;

      
        foreach($accounts as $account) {
            $this->out("Configuring data {$count}  for account {$account->account_id}.");           
            $account_data = $this->FacebookAdAccounts->getAccount($userdata, $account);
            if(!empty($account_data)) {
                $this->FacebookAdAccounts->UpdateAccount($account_data, $account);
            } else {
                $this->FacebookAdAccounts->addAccount($userdata, $account);
            }
            $this->insertUpdateCampaign($account, $userdata, $last_update);
            $this->DownloadCustomConversion($userdata->user_id, $account->id);
            $count++;
        }
        //Check is_deleted status(ad,adset,campaign)
        $this->CheckStatus($userdata->user_id, $last_update);
    }

    public function insertUpdateCampaign($account, $userdata, $last_update) {
        $this->out("insertUpdateCampaign...");
        $account_id = $account->account_id;
        $account    = new AdAccount('act_'.$account_id);
        //Get CampaignFields
        $fields     = $this->FacebookCampaigns->getCampaignFields();
        $campaigns  = $account->getCampaigns($fields, $params=array());

        if(!empty($campaigns)) {
            foreach($campaigns as $campaign) {
                $campaign_id = $campaign->id;
                $campaign_data = $this->FacebookCampaigns->getCampaign($account_id, $campaign);
                if(!empty($campaign_data)) {
                    $this->FacebookCampaigns->UpdateCampaign($campaign_data, $campaign, $last_update);
                } else {
                    $this->FacebookCampaigns->addCampaign($account_id, $campaign, $userdata, $last_update);
                }

                //Get AdFields
                $AdFields = $this->FacebookAds->getAdFields();
                $Campaign = new Campaign($campaign_id);
                $ads = $Campaign->getAds($AdFields);
                if(!empty($ads)) {
                    foreach($ads as $ad) {
                        $adset_id = $ad->adset_id;
                        $id = $this->insertUpdateAd($account_id, $campaign_id, $adset_id, $ad, $userdata, $last_update);
                    }
                }
            }
            $this->insertUpdateAdsets($account_id, $userdata, $last_update);
        }
    }

    public function insertUpdateAdsets($account_id, $userdata, $last_update) {
        $this->out("insertUpdateAdsets...");
        $account = new AdAccount('act_'.$account_id);
        $adset_fields = $this->FacebookAdsets->getAdSetFields();
        $adsets = $account->getAdSets($adset_fields, $params=array());
        if(!empty($adsets)) {
            foreach($adsets as $adset) {
                $adset_data = $this->FacebookAdsets->getAdset($account_id, $adset);
                if(!empty($adset_data)) {
                    $this->FacebookAdsets->UpdateAdset($adset_data, $adset, $last_update);
                } else {
                    $this->FacebookAdsets->addAdset($adset, $userdata, $last_update);
                }
            }
        }
    }

    public function insertUpdateAd($account_id, $campaign_id, $adset_id, $ad, $userdata, $last_update) {
        $this->out("insertUpdateAd...");        
        $ad_id = $ad->id;
        $date = date('Y-m-d');

        //Check ads info exist or not in database
        $ad_data = $this->FacebookAds->getAd($account_id, $campaign_id, $adset_id, $ad_id, $date);
        if(!empty($ad_data)) {
            $this->FacebookAds->UpdateAd($ad_data, $ad, $last_update);
            $id = $ad_data[0]->id;
        } else {
            $result = $this->FacebookAds->addAd($ad, $date, $userdata, $last_update);
            $id = $result;
        }
    }

    //CustomConversion Script
    public function DownloadCustomConversion($user_id, $account_id) {
        $this->out("Check CustomConversion....");   
        $accounts = $this->FacebookAdAccounts->getOne($user_id, $account_id);

        //Get Fields
        $fields =$this->FacebookCustomConversions->getCustomConversionFields();

        if(empty($accounts))
            return false;

        foreach($accounts as $key => $account) {
            $account_id = $account->account_id;
            $CustomConversion = new CustomConversion("act_".$account_id);
            $conversions = $CustomConversion->getSelf($fields, [], false);
            $count = 1;
            if(empty($conversions))
                foreach ($conversions->data as $conversion) {
                   $custom_conversions = $this->FacebookCustomConversions->getRecord($conversion['account_id'], $conversion['id']);

                if(empty($custom_conversions))
                    $id = $this->FacebookCustomConversions->add($conversion, $count);

                $count++;
            }
        }
    }

    public function CheckStatus($user_id, $last_updated){
        $this->out("Check Deleted objects....");   
        $this->FacebookCampaigns->setAccountStatus($status="DELETED", $user_id, $last_updated);
        $this->FacebookAds->setAccountStatus($status="DELETED", $user_id, $last_updated);
        $this->FacebookAdsets->setAccountStatus($status="DELETED", $user_id, $last_updated);
    }
}
