<?php

namespace App\View\Helper;
use Cake\View\Helper;
class DevExpressDataGridHelper extends Helper {

    public function columnsconfiguration($columns_config_data)
    {
        $columns_config_arr = array();
        $string = 'calculateCustomSummary: function(options) { ';
        $count = 0;
        foreach($columns_config_data as $key => $columns_config) {
            if($columns_config->show == 1) {
                $expression = $columns_config->expression;

                if($columns_config->type == 'Core') {
                    $name = $columns_config->db_name.'_core';
                    $var = $columns_config->db_name;
                    $calculate1 = " options.totalValue += options.value; ";
                    $calculate2 = " $var = options.totalValue; ";
                } else {
                    $name = $columns_config->user_name.'_custom';
                    $var = 'total_'.$columns_config->user_name;
                    $calculate1 = "";
                    $calculate2 = " options.totalValue = eval('$expression'); ";
                }

                $columns_config_arr[$count]['column'] = $columns_config->user_name;
                $columns_config_arr[$count]['displayFormat'] = '{0}';
                $columns_config_arr[$count]['alignment'] = 'top';
                $columns_config_arr[$count]['showInColumn'] = $columns_config->user_name;
                $columns_config_arr[$count]['summaryType'] = 'custom';
                $columns_config_arr[$count]['valueFormat'] = 'fixedPoint';
                //$columns_config_arr[$count]['precision'] = '2';
                $columns_config_arr[$count]['name'] = $name;

                $string .= " if(options.name == '$name') { ";
                $string .= "   if(options.summaryProcess == 'start') {"; 
                $string .= " options.totalValue = 0; ";
                $string .= " } ";
                $string .= " if(options.summaryProcess == 'calculate') { ";
                $string .= " $calculate1 ";
                $string .= " $calculate2 ";
                $string .= "  } ";
                $string .= "   if(options.summaryProcess == 'finalize') { ";
                $string .= "  options.totalValue = options.totalValue; ";
                $string .= "  }";
                $string .= "  }";

                $count++;
            }
        }
        $string .= "  }";
        return ["columns_config_arr"=>$columns_config_arr,"string"=> $string];
        exit;
    }
}
?>