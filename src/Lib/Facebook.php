<?php namespace App\Lib;

    class Facebook {
        private $last_response_headers = null;
        private $access_token = null;

        public function __construct() {
            /*$this->app_id = "";
            $this->app_secret = "";
            $this->redirect_url = "";*/
        }

        public function setAccessToken() {
            $this->access_token = 
"CAAVSt2bI46sBABWcL4WJlwSbmbZBrcXhXOBA3zgWoAzptjcxjA1H92PkHUaNqwMQebZC7ZCZAEvHJwb1TLuJcmvIUYcofPlK0HT55k3aOOrZAZB0AtVDq2xeW0kbiCWY4d5ZBWJRMhfy4BB9SgWEq3jZC0shFH3ENTRunF3Ruo4CwgMsX9PJap3S";
        }

        public function getAllAccounts($access_token) {
            $url = "https://graph.facebook.com/v2.10/me/adaccounts?fields=id,account_id,account_status,name&access_token=$access_token";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            //echo "<pre>"; print_r($response_arr); die;

            if(!empty($response_arr['data']))
                return $response;
        }

        public function getAdsets($account_id) {
            $url = "https://graph.facebook.com/v2.10/act_$account_id/adsets?fields=id,account_id,campaign_id,name,status&access_token={$this->access_token}";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            //echo "<pre>"; print_r($response_arr); die;

            if(!empty($response_arr['data']))
                return $response;
        }

        public function getCampaigns($account_id) {
            $url = "https://graph.facebook.com/v2.10/act_$account_id/campaigns?fields=id,account_id,name,objective,status&access_token={$this->access_token}";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            //echo "<pre>"; print_r($response_arr); die;

            if(!empty($response_arr['data']))
                return $response;
        }

        public function getAds($campaign_id) {
            $url = "https://graph.facebook.com/v2.10/$campaign_id/ads?fields=id,bid_amount,bid_info,bid_type,account_id,campaign_id,adset_id,name,configured_status,effective_status,status,creative,created_time&access_token={$this->access_token}";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            //echo "<pre>"; print_r($response_arr); die;

            if(!empty($response_arr['data']))
                return $response;
        }

        public function getAdInsights($ad, $date, $attr_window) {
            $ad_id = $ad->id;

            $url = "https://graph.facebook.com/v2.10/$ad_id/insights?fields=account_id,account_name,campaign_name,adset_name,ad_id,ad_name,clicks,frequency,impressions,reach,spend,actions,action_values&time_range={'since':'$date','until':'$date'}&time_increment=all_days&action_attribution_windows=['$attr_window']&access_token={$this->access_token}";

            //$url = "https://graph.facebook.com/v2.10/6101438720382/insights?fields=account_name,campaign_name,adset_name,ad_id,ad_name,clicks,frequency,impressions,reach,spend,actions,action_values&time_range={'since':'2018-01-01','until':'2018-04-24'}&time_increment=all_days&action_attribution_windows=['$attr_window']&access_token=CAAVSt2bI46sBABWcL4WJlwSbmbZBrcXhXOBA3zgWoAzptjcxjA1H92PkHUaNqwMQebZC7ZCZAEvHJwb1TLuJcmvIUYcofPlK0HT55k3aOOrZAZB0AtVDq2xeW0kbiCWY4d5ZBWJRMhfy4BB9SgWEq3jZC0shFH3ENTRunF3Ruo4CwgMsX9PJap3S";

            //echo $url; die;

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            //echo "<pre>"; print_r($response); die;

            if(!empty($response_arr['data']))
                return $response;
        }

        public function CustomConversion($account_id) {
            $url = "https://graph.facebook.com/v2.12/act_$account_id/customconversions?fields=id,account_id,aggregation_rule,creation_time,custom_event_type,data_sources,default_conversion_value,description,event_source_type,first_fired_time,is_archived,last_fired_time,name,offline_conversion_data_set,pixel,retention_days,rule&access_token={$this->access_token}";

            //echo $url;

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            //echo "<pre>"; print_r($response); die;

            if(!empty($response_arr['data']))
                return $response;
        }

        private function curlHttpApiRequest($method, $url, $payload = '', $request_headers = array()) {
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

            if($method == 'POST') {
                if(is_array($payload)) $payload = http_build_query($payload);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            } else if($method == 'PUT') {
                $payload = json_encode($payload);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            }

            if(!empty($request_headers)) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            }

            $response = curl_exec($ch);

            curl_close($ch);

            list($message_headers, $message_body) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
            $this->last_response_headers = $this->curlParseHeaders($message_headers);

            return $message_body;
        }

        public function curlParseHeaders($message_headers)
        {
            $header_lines = preg_split("/\r\n|\n|\r/", $message_headers);
            $headers = array();
            list(, $headers['http_status_code'], $headers['http_status_message']) = explode(' ', trim(array_shift($header_lines)), 3);
            foreach($header_lines as $header_line)
            {
                list($name, $value) = explode(':', $header_line, 2);
                $name = strtolower($name);
                $headers[$name] = trim($value);
            }

            return $headers;
        }
        
        public function createApiCall($method, $params=null, $url){
            $ch = curl_init("$url");
            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

            if(is_array($params)) $params = http_build_query($params);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            $response = curl_exec($ch);
            return $response;
        }
    }
?>