<?php namespace App\Lib;

    class Snapchat {
        private $last_response_headers = null;

        public function __construct() {
            $this->client_id = "650ea430-b5f0-4673-8a7e-e08af9a9964d";
            $this->client_secret = "db095d04786136c418aa";
            $this->redirect_url = "https://snapchat.pareto.solutions/snapchat/login";
        }

        public function createButton() {
            return "<p>Please <a href='/snapchat/auth-login-redirect'>Click Here</a> to authorize snapchat account</p>";
        }

        public function authRedirectURL() {
            return "https://accounts.snapchat.com/accounts/oauth2/auth?response_type=code&client_id={$this->client_id}&redirect_uri={$this->redirect_url}&scope=snapchat-marketing-api";
        }

        public function getAccessToken($code, $scope) {
            $url = "https://accounts.snapchat.com/login/oauth2/access_token";

            $payload = "client_id={$this->client_id}&client_secret={$this->client_secret}&code=$code&grant_type=$scope";
            $request_headers = array();

            $response = $this->curlHttpApiRequest('POST', $url, $payload, $request_headers);
            $access_token_arr = json_decode($response, true);
            return $access_token_arr;
        }

        public function getAuthenticatedUser($access_token) {
            $url = "https://adsapi.snapchat.com/v1/me";

            $request_headers = array("Content-Type: application/josn; charset=utf-8", 'Expect:');
            $request_headers[] = "Authorization: Bearer $access_token";

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            if($response_arr['request_status'] == 'SUCCESS')
                return $response_arr;
            else if($this->last_response_headers['http_status_code'] == 401)
                return 'invalid_token';
        }

        public function getOrganizations($access_token) {
            $url = "https://adsapi.snapchat.com/v1/me/organizations";

            $request_headers = array("Content-Type: application/josn; charset=utf-8", 'Expect:');

            // add auth headers
            $request_headers[] = "Authorization: Bearer $access_token";

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            if($response_arr['request_status'] == 'SUCCESS')
                return $response_arr['organizations'][0]['organization']['id'];
            else if($this->last_response_headers['http_status_code'] == 401)
                return 'invalid_token';
        }

        public function getAdAccounts($access_token, $organization_id) {
            $url = "https://adsapi.snapchat.com/v1/organizations/$organization_id/adaccounts";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            // add auth headers
            $request_headers[] = "Authorization: Bearer $access_token";

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            if($response_arr['request_status'] == 'SUCCESS')
                return $response_arr;
        }

        public function getAdAccountCampaigns($access_token, $ad_account_id) {
            $url = "https://adsapi.snapchat.com/v1/adaccounts/$ad_account_id/campaigns";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            // add auth headers
            $request_headers[] = "Authorization: Bearer $access_token";

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            if($response_arr['request_status'] == 'SUCCESS')
                return $response_arr;
        }

        public function getAdAccountCampaignAdSquads($access_token, $campaign_id) {
            $url = "https://adsapi.snapchat.com/v1/campaigns/$campaign_id/adsquads";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            // add auth headers
            $request_headers[] = "Authorization: Bearer $access_token";

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            if($response_arr['request_status'] == 'SUCCESS')
                return $response_arr;
        }

        public function getAdSquadAds($access_token, $ad_squad_id) {
            $url = "https://adsapi.snapchat.com/v1/adsquads/$ad_squad_id/ads";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            // add auth headers
            $request_headers[] = "Authorization: Bearer $access_token";

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            if($response_arr['request_status'] == 'SUCCESS')
                return $response_arr;
        }

        public function getAdStatus($access_token, $ad_id) {
            $url = "https://adsapi.snapchat.com/v1/ads/$ad_id/stats";

            $request_headers = array("Content-Type: text/html; charset=utf-8", 'Expect:');

            // add auth headers
            $request_headers[] = "Authorization: Bearer $access_token";

            $payload = array();
            $response = $this->curlHttpApiRequest('GET', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);

            if($response_arr['request_status'] == 'SUCCESS')
                return $response_arr;
        }

        public function UpdateCampaign($access_token, $ad_account_id, $params) {
            $url = "https://adsapi.snapchat.com/v1/adaccounts/$ad_account_id/campaigns";

            $payload = $params;

            $request_headers = array("Content-Type: application/json; charset=utf-8");
            $request_headers[] = "Authorization: Bearer $access_token";

            $response = $this->curlHttpApiRequest('PUT', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);
            return $response_arr;
        }

        public function UpdateAdSquad($access_token, $campaign_id, $params) {
            $url = "https://adsapi.snapchat.com/v1/campaigns/$campaign_id/adsquads";

            $payload = $params;

            $request_headers = array("Content-Type: application/json; charset=utf-8");
            $request_headers[] = "Authorization: Bearer $access_token";

            $response = $this->curlHttpApiRequest('PUT', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);
            return $response_arr;
        }

        public function UpdateAd($access_token, $ad_squad_id, $params) {
            $url = "https://adsapi.snapchat.com/v1/adsquads/$ad_squad_id/ads";

            $payload = $params;

            $request_headers = array("Content-Type: application/json; charset=utf-8");
            $request_headers[] = "Authorization: Bearer $access_token";

            $response = $this->curlHttpApiRequest('PUT', $url, $payload, $request_headers);
            $response_arr = json_decode($response, true);
            return $response_arr;
        }

        private function curlHttpApiRequest($method, $url, $payload = '', $request_headers = array()) {
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

            if($method == 'POST') {
                if (is_array($payload)) $payload = http_build_query($payload);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            } else if($method == 'PUT') {
                $payload = json_encode($payload);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            }

            if(!empty($request_headers)) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            }

            $response = curl_exec($ch);

            curl_close($ch);

            list($message_headers, $message_body) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
            $this->last_response_headers = $this->curlParseHeaders($message_headers);

            return $message_body;
        }

        public function curlParseHeaders($message_headers)
        {
            $header_lines = preg_split("/\r\n|\n|\r/", $message_headers);
            $headers = array();
            list(, $headers['http_status_code'], $headers['http_status_message']) = explode(' ', trim(array_shift($header_lines)), 3);
            foreach ($header_lines as $header_line)
            {
                list($name, $value) = explode(':', $header_line, 2);
                $name = strtolower($name);
                $headers[$name] = trim($value);
            }

            return $headers;
        }
    }
?>