 <?= $this->Html->css('common/base.css') ?>
<div style="width: 800px; margin-left: 260px; margin-top: 100px;">
    <h1>Create Your Account</h1>

    <?php
        echo $this->Flash->render('auth');
        echo $this->Form->create($user);
        echo $this->Form->input('full_name');
        echo $this->Form->input('email');
        echo $this->Form->input('password');
        echo $this->Form->input('password2', array('label' => "Confirm Password", 'type' => 'password'));
        echo $this->Form->button(__('Sign Up'));
        echo $this->Form->end();
    ?>

    <p>If you already have an account <?= $this->Html->link('Click Here', ['action' => 'index']) ?> to sign In</p>
</div>
<?php  $this->start('footer'); echo $this->element('footer/footer'); $this->end(); ?>