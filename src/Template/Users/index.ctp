 <?= $this->Html->css('common/base.css') ?>
<div style="width: 800px; margin-left: 260px; margin-top: 100px;">
    <h1>Sign In</h1>

    <?php echo $this->Form->create($user);
        echo $this->Form->input('email');
        echo $this->Form->input('password');
        echo $this->Form->button('Login');
        echo $this->Form->end() 
    ?>

    <p><?= $this->Html->link('Create an account', ['action' => 'add']) ?></p>
</div>
<?php  $this->start('footer'); echo $this->element('footer/footer'); $this->end(); ?>