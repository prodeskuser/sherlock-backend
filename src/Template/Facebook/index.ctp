<?php if(empty($_GET['token']) || $_GET['token']!="HTYC7DG2"){
    echo "<h1>You are not authorized to access this url.</h1> Access denied";
    return false;
}?>
<?php
    echo $this->Html->css(['dx.common.css', 'dx.spa.css', 'bootstrap.min.css', 'bootstrap-datetimepicker.css']);
?>
<style>
   .error{
        color: #a94442;
    }
  </style>
    <div class="container">
        </br>
        <div class="row">
            <ul class="nav nav-tabs nav-justified">
              <li class="active"><a data-toggle="tab" href="#menu1">Create Offer & Adset</a></li>
              <li><a data-toggle="tab" href="#menu2">Duplicate Mobile Ad</a></li>
              <li><a data-toggle="tab" href="#menu3">Create Offer & Adset from Existing</a></li>
            </ul>
        </div>

        <div class="row">
            <div class="tab-content">
                <div id="menu1" class="tab-pane fade in active">
                    <form action="post_data.php" name="CreateAdsetForm" id="CreateAdsetForm" method="POST">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading text-center"><h5>Create an Ad Set</h5></div>-->
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <input type="hidden" name="adsetForm" value="adsetForm" />
                                        <div class="form-group alrady_have_offer">
                                            <label for="location_type">Location Type:</label>
                                            <select class="form-control" name="location_type" id="location_type">
                                                <option selected value="online">Online</option>
                                                <option value="offline">Offline</option>
                                            </select>
                                        </div>
                                        <div class="form-group alrady_have_offer">
                                            <label for="type">Description :</label>
                                            <textarea  class="form-control" id="description" placeholder="Enter Offer Description" name="description" required >Enter the text description for this offer</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="type">Offer Store URL:</label>
                                            <input type="text" class="form-control" id="redemption_link" placeholder="Enter OFFER URL" name="redemption_link" value="https://play.google.com/store/apps/details?id=com.Boom.StarLeague" required />
                                        </div>
                                        <div class="form-group alrady_have_offer">
                                            <label for="type">Offer Redemption Code:</label>
                                            <input type="text" class="form-control" id="redemption_code" placeholder="Enter Redemption Code" name="redemption_code" value="" required />
                                        </div>
                                        <div class="form-group alrady_have_offer">
                                            <label for="details">Offer Details:</label>
                                            <textarea  class="form-control" id="details" placeholder="Enter details for this specific offer" name="details" required>Enter the details for this specific offer</textarea>
                                        </div>
                                        <div class="form-group alrady_have_offer">
                                            <label for="expiration_time">Offer Expiration Time:</label>
                                            <input type="text" class="form-control datepicker" id="expiration_time" placeholder="Enter Expiration Time" name="expiration_time" value="" required />
                                        </div>
                                    </div>
                                    <div class="col-sm-8"> 
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                              <label for="application_id">Account Id:</label>
                                              <input type="text" class="form-control" id="account_id" placeholder="Enter Account Id" name="account_id" value="1243846285671192" required />
                                            </div>
                                            <div class="form-group">
                                              <label for="adset_name">New Adset Name:</label>
                                              <input type="text" class="form-control" id="ad_set_name" placeholder="Enter Ad Set Title" name="name" value="MyOfferClaimAdSet" required />
                                            </div>
                                            <div class="form-group">
                                              <label for="lifetime_budget">Lifetime Budget:</label>
                                              <input type="text" class="form-control" id="lifetime_budget" placeholder="Enter Lifetime Budget" name="lifetime_budget" value="150000" required />
                                            </div>
                                            <div class="form-group">
                                              <label for="start_time">Start Time:</label>
                                              <input type="text" class="form-control datepicker" id="start_time" placeholder="Enter start time" name="start_time" value="" />
                                            </div>
                                            <div class="form-group">
                                              <label for="end_time">End Time:</label>
                                              <input type="text" class="form-control datepicker" id="end_time" placeholder="Enter End Time" name="end_time" value= "" />
                                            </div>
                                            <div class="form-group">
                                              <label for="bid_amount">Access Token:</label>
                                              <textarea  class="form-control" id="access_token" placeholder="Enter Access Token" name="access_token" rows="3" required>CAAVSt2bI46sBABWcL4WJlwSbmbZBrcXhXOBA3zgWoAzptjcxjA1H92PkHUaNqwMQebZC7ZCZAEvHJwb1TLuJcmvIUYcofPlK0HT55k3aOOrZAZB0AtVDq2xeW0kbiCWY4d5ZBWJRMhfy4BB9SgWEq3jZC0shFH3ENTRunF3Ruo4CwgMsX9PJap3S</textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-6"> 
                                            <div class="form-group">
                                              <label for="campaign_id">Campaign Id To Deploy:</label>
                                              <input type="text" class="form-control" id="campaign_id" placeholder="Enter Ad Campaign Id" name="campaign_id" value="23842874736280277" required />
                                            </div>
                                            <div class="form-group">
                                              <label for="billing_event">Billing Event:</label>
                                              <input type="text" class="form-control" id="billing_event" placeholder="Enter Billing Event" name="billing_event" value="IMPRESSIONS" />
                                            </div> 
                                            <div class="form-group">
                                              <label for="optimization_goal">Optimization Goal:</label>
                                              <input type="text" class="form-control" id="optimization_goal" placeholder="Enter Optimization Goal" name="optimization_goal" value="LINK_CLICKS" />
                                            </div>
                                            <div class="form-group">
                                              <label for="bid_amount">Bid Amount:</label>
                                              <input type="text" class="form-control" id="bid_amount" placeholder="Enter Bit Amount" name="bid_amount" value="100" />
                                            </div>
                                            <div class="form-group">
                                              <label for="page_id">Page Id:</label>
                                              <input type="text" class="form-control" id="page_id" placeholder="Enter Page Id" name="page_id" value= "1879848062229011" required />
                                            </div>
                                            <div class="form-group">
                                              <label for="application_id">Application Id:</label>
                                              <input type="text" class="form-control" id="application_id" placeholder="Enter Offer Id" name="application_id" value= "218137531924199" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                                <div class="col-sm-12" style="display:none" id="result">
                                    <h1>
                                        Result:
                                    <h1>
                                    <pre id="result_pre">
                                    </pre>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <form action="post_data.php" name="adForm" id="adForm" method="POST">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading text-center"><h5>Create an Ad Set</h5></div>-->
                            <div class="panel-body">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="adset_id">Original Ad id:</label>
                                        <input type="text" class="form-control" placeholder="Enter Ad Id" name="ad_id" value="" required />
                                        <input type="hidden" name="adForm" value="adForm" />
                                    </div>
                                    <div class="form-group">
                                        <label for="adset_id">Deploy to Adset Id:</label>
                                        <input type="text" class="form-control" placeholder="Enter Adset Id" name="adset_id" value="23842874736300277" required />
                                    </div>
                                    <div class="form-group">
                                        <label for="access_token">Access Token:</label>
                                        <textarea type="text" class="form-control" placeholder="Enter Access Token" rows="5" name="access_token" value="" required />CAAVSt2bI46sBABWcL4WJlwSbmbZBrcXhXOBA3zgWoAzptjcxjA1H92PkHUaNqwMQebZC7ZCZAEvHJwb1TLuJcmvIUYcofPlK0HT55k3aOOrZAZB0AtVDq2xeW0kbiCWY4d5ZBWJRMhfy4BB9SgWEq3jZC0shFH3ENTRunF3Ruo4CwgMsX9PJap3S</textarea>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success pull-right">Submit</button>
                                    </div>
                                </div>
                                <div class="col-sm-8" style="display:none" id="resultadForm">
                                     <h1>
                                        Result:
                                    <h1>
                                    <pre id="resultadForm_pre">
                                    </pre>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <form action="post_data.php" name="copiesadsetForm" id="copiesadsetForm" method="POST">
                        <div class="panel panel-default">
                            <!--<div class="panel-heading text-center"><h5>Create an Ad Set</h5></div>-->
                            <div class="panel-body">
                                <div class="col-sm-4">
                                    <div class="form-group alrady_have_offer">
                                        <label for="location_type">Location Type:</label>
                                        <select class="form-control" name="location_type">
                                            <option selected value="online">Online</option>
                                            <option value="offline">Offline</option>
                                        </select>
                                    </div>
                                    <div class="form-group alrady_have_offer">
                                        <label for="type">Description :</label>
                                        <textarea  class="form-control" placeholder="Enter Offer Description" name="description" required >Enter the text description for this offer</textarea>
                                    </div>
                                    <div class="form-group alrady_have_offer">
                                        <label for="type">Offer Redemption Code:</label>
                                        <input type="text" class="form-control" placeholder="Enter Redemption Code" name="redemption_code" value="" required />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group alrady_have_offer">
                                        <label for="details">Offer Details:</label>
                                        <textarea  class="form-control"  placeholder="Enter details for this specific offer" name="details" required>Enter the details for this specific offer</textarea>
                                    </div>
                                    <div class="form-group alrady_have_offer">
                                        <label for="expiration_time">Offer Expiration Time:</label>
                                        <input type="text" class="form-control datepicker" id="expiration_time" placeholder="Enter Expiration Time" name="expiration_time" value="" required />
                                    </div>
                                    <div class="form-group">
                                      <label for="page_id">Page Id:</label>
                                      <input type="text" class="form-control" id="page_id" placeholder="Enter Page Id" name="page_id" value= "1879848062229011" required />
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="campaign_d">New Adset Name:</label>
                                        <input type="text" class="form-control" placeholder="Enter AdSet Name" name="adset_name" value="" required />
                                    </div>
                                    <div class="form-group alrady_have_offer">
                                        <label for="type">Original Adset Id:</label>
                                        <input type="text" class="form-control" placeholder="Enter Adset Id" name="ad_set_id" value="" required />
                                        <input type="hidden" name="adsetcopiesForm" value="adsetcopiesForm" />
                                    </div>
                                    <div class="form-group">
                                      <label for="campaign_d">Campaign Id To Deploy:</label>
                                      <input type="text" class="form-control" placeholder="Enter Campaign Id" name="campaign_id" value= "" required />
                                    </div>
                                    <div class="form-group">
                                        <label for="access_token">Access Token:</label>
                                        <textarea type="text" class="form-control" placeholder="Enter Access Token" rows="5" name="access_token" value="" required />CAAVSt2bI46sBABWcL4WJlwSbmbZBrcXhXOBA3zgWoAzptjcxjA1H92PkHUaNqwMQebZC7ZCZAEvHJwb1TLuJcmvIUYcofPlK0HT55k3aOOrZAZB0AtVDq2xeW0kbiCWY4d5ZBWJRMhfy4BB9SgWEq3jZC0shFH3ENTRunF3Ruo4CwgMsX9PJap3S</textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label><input name="duplicate_ads" type="checkbox" value="1">Duplicate Ads</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success pull-right">Submit</button>
                                </div>
                                </br>
                                <div class="col-sm-12" style="display:none" id="resultcopyadsetForm">
                                     <h1>
                                        Result: 
                                    <h1>
                                    <pre id="resultcopyadsetForm_pre">
                                    </pre>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php    
    echo $this->Html->script(['jquery-3.1.0.min', 'bootstrap.min.js', 'bootstrap-datetimepicker.min.js', 'jquery.validate.min.js']);
?>
    <script>
        $( document ).ready( function () {
            $( function() {
                $( ".datepicker" ).datetimepicker({
                    format: "dd-mm-yyyy hh:ii:ss",
                    autoclose: true,
                    todayBtn: true,
                    pickerPosition: "bottom-left"
                   });
            } );

            $( "#CreateAdsetForm" ).submit(function( event )  {
                if($("#CreateAdsetForm").valid()){
                    event.preventDefault();
                    var formData = $( "#CreateAdsetForm").serialize();
                    $.ajax({
                        url: '<?php echo "facebook/fbutils"; ?>',
                        type: "post",
                        data: formData,
                        beforeSend: function(){
                           $("body").css({"opacity": "0.5"});
                           $(".btn btn-primary").prop('disabled', true);
                        },
                        success: function(res) {
                            $("body").css({"opacity": "1"});
                            $(".btn btn-primary").prop('disabled', false);
                            $("#result").show();
                            $("#result_pre").html(res);
                            $('html, body').animate({
                                scrollTop: $("#result_pre").offset().top
                            }, 1500);
                        },
                        error: function(xhr, status, error) {
                            $("body").css({"opacity": "1"});
                            $(".btn btn-primary").prop('disabled', false);
                        },
                        complete: function(){
                           $("body").css({"opacity": "1"});
                           $(".btn btn-primary").prop('disabled', false);
                        }
                    });
                };
            });
            $( "#CreateAdsetForm" ).validate();
            $( "#adForm" ).validate();
            $('#adForm').submit(function( event )  {
                if($("#adForm").valid()){
                    event.preventDefault();
                    var formData = $( "#adForm").serialize();
                    $.ajax({
                        url: '<?php echo "facebook/fbutils"; ?>',
                        type: "post",
                        data: formData,
                        beforeSend: function(){
                           $("body").css({"opacity": "0.5"});
                           $(".btn btn-success").prop('disabled', true);
                        },
                        success: function(res) {
                            $("body").css({"opacity": "1"});
                            $(".btn btn-success").prop('disabled', false);
                            $("#resultadForm").show();
                            $("#resultadForm_pre").html(res);
                        },
                        error: function(xhr, status, error) {
                            $("body").css({"opacity": "1"});
                            $(".btn btn-success").prop('disabled', false);
                        },
                        complete: function(){
                           $("body").css({"opacity": "1"});
                           $(".btn btn-success").prop('disabled', false);
                        }
                    });
                }
            } );
            $("#copiesadsetForm").validate();
            $('#copiesadsetForm').submit(function( event )  {
                if($("#copiesadsetForm").valid()){
                    event.preventDefault();
                    var formData = $( "#copiesadsetForm").serialize();
                    $.ajax({
                        url: '<?php echo "facebook/fbutils"; ?>',
                        type: "post",
                        data: formData,
                        beforeSend: function(){
                           $("body").css({"opacity": "0.5"});
                           $(".btn btn-success").prop('disabled', true);
                        },
                        success: function(res) {
                            $("body").css({"opacity": "1"});
                            $(".btn btn-success").prop('disabled', false);
                            $("#resultcopyadsetForm").show();
                            $("#resultcopyadsetForm_pre").html(res);
                            $('html, body').animate({
                                scrollTop: $("#resultcopyadsetForm_pre").offset().top
                            }, 1500);
                        },
                        error: function(xhr, status, error) {
                            $("body").css({"opacity": "1"});
                            $(".btn btn-success").prop('disabled', false);
                        },
                        complete: function(){
                           $("body").css({"opacity": "1"});
                           $(".btn btn-success").prop('disabled', false);
                        }
                    });
                }
            } );
		});
        
    </script>

  