<div id="sidebar-wrapper">
    <a href="#" class="left-arrow-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i><i style="display:none;" class="fa fa-angle-double-left" aria-hidden="true"></i></a>
    <div class="campaign-name select">Campaign name <span>SELECTED</span></div>
    <div id="accordion">
        <div class="accordion-nav">
            <div class="accordion-link-title">
                <a class="ard-link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="fa fa-plus"></span> What is HTML?</a>
            </div>
            <div id="collapseOne" class="panel-collapse collapse accordion-sub-menu in">
                <ul>
                    <li><a href="#">Ads</a></li>
                    <li><a href="#">Ads</a></li>
                    <li><a href="#">Ads</a></li>
                    <li><a href="#">Ads</a></li>
                </ul>
            </div>
        </div>
        <div class="accordion-nav">
            <div class="accordion-link-title">
                <a class="ard-link" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo"><span class="fa fa-plus"></span> What is HTML?</a>
            </div>
            <div id="collapsetwo" class="panel-collapse collapse accordion-sub-menu">
                <ul>
                    <li><a href="#">Ads</a></li>
                    <li><a href="#">Ads</a></li>
                    <li><a href="#">Ads</a></li>
                    <li><a href="#">Ads</a></li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="sidebar-nav">
        <li> <a href="#">Campaign Name</a> </li>
        <li> <a href="#">Campaign Name</a> </li>
        <li> <a href="#">Campaign Name</a> </li>
        <li> <a href="#">Campaign Name</a> </li>
    </ul>
</div>