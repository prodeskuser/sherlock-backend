<li class="nav-item accountbox">
    <?= $this->Form->create(null, ['class' => 'form-inline my-2 my-lg-0 mr-lg-2']); ?>
        <div class="input-group">
            <i class="fa fa-user" aria-hidden="true"></i> 
            <?= $this->Form->select('account_id', [], ['id' => 'account_id', 'class' => 'report_filter']); ?>
        </div>
    <?= $this->Form->end(); ?>
</li>

<li class="nav-item dropdown alert-bx">
    <a class="nav-link dropdown-toggle mr-lg-2 alerticon" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-bell-o" aria-hidden="true"></i> <i class="fa fa-fw fa-circle"></i>
        <span class="d-lg-none">Alerts
            <span class="badge badge-pill badge-warning">6 New </span>
        </span>
    </a>
    <!-- start section for alert message -->
    <div class="dropdown-menu" aria-labelledby="alertsDropdown">
        <h6 class="dropdown-header">New Alerts:</h6>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
            <span class="text-success"><i class="fa fa-long-arrow-up fa-fw"></i>Status Update</span>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
            <span class="text-danger"><i class="fa fa-long-arrow-down fa-fw"></i>Status Update</span>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
            <span class="text-success"><i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong></span>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item small" href="#">View all alerts</a>
    </div>
</li>
<li class="nav-item dropdown user-setting">
    <a class=" " id="userdropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-img"><img src="images/profile_picture.png" alt="" /></span> <i class="fa fa-angle-down" aria-hidden="true"></i></a>	
    
    <div class="dropdown-menu">
        <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-fw fa-sign-out')).'Logout', array('controller' => 'Users', 'action' => 'logout'), array('escape' => false, 'class' => 'dropdown-item')) ?>
    </div>
</li>