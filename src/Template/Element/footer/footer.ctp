<div class="footer-bg footer-top">
    <div class="text-center">
        <div class="col-md-4 offset-md-4">
            <div class="help-box">
                <div class="quesiocn"><i class="fa fa-question-circle" aria-hidden="true"></i></div>
                <div class="h3">Need Help?</div>
                <div class="help-content">We are pleased to help you, Please send us your question to <span>help@sherlock.com</span>, or call us to <span>0203456122</span></div>
            </div>
        </div>
    </div>
</div>
<div class="footer-copyright">
    All rights reserved - 2018 - Pareto's Properties
</div>