<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Pareto Solutions';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->Html->css(['bootstrap/bootstrap.min', 'bootstrap/responsive.bootstrap4.min','bootstrap/font-awesome.min', 'bootstrap/font.css', 'dxdatagrid/dx.common', 'dxdatagrid/dx.light', 'dxdatagrid/dx.spa', 'date-picker/daterangepicker.css', 'jquery/jquery-ui.css', 'common/global_style', ]) ?>
    <?= $this->Html->script(['jquery/jquery.min']) ?>
</head>
<body class="">
    <div class="wrapper" id="wrapper">
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top pareto-bar" data-topbar role="navigation">
            <a href="#menu-toggle" title="Manage" class="side-toggle-menu" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
           <?= (($authUser) ? $this->Html->link($this->Html->image('/images/logo.png',array('alt'=>'Pareto Solutions', 'title'=>'Pareto Solutionse','class'=>'img-responsive')), ['controller' => 'Reports','action' => 'index',],['escape' => false]) : $this->Html->link($this->Html->image('/images/logo.png',array('alt'=>'Pareto Solutions', 'title'=>'Pareto Solutionse','class'=>'img-responsive')), ['controller' => 'Users','action' => 'index',],['escape' => false])); ?>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <?php if($authUser) { ?>
                    <?= $this->fetch('navbar') ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="full-width-menu">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2">
                            <h3 class="title" title="Configuration"><i class="fa fa-list-ul" aria-hidden="true"></i>Config</h3>
                            <div class="inner-content">
                                <ul class="menu-01">
                                    <li> <?php echo $this->Html->link('Custom Columns', array('controller' => 'UserColumns', 'action' => 'index'),['Title' => 'Add Custom Column', 'target' => '_blank']); ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h3 class="title"><i class="fa fa-signal" aria-hidden="true"></i>Create & manage</h3>
                            <div class="inner-content">
                                <ul class="menu-01">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h3 class="title"><i class="fa fa-bar-chart" aria-hidden="true"></i>measure & report</h3>
                            <div class="inner-content">
                                <ul class="menu-01">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h3 class="title"><i class="fa fa-th-large" aria-hidden="true"></i>assets</h3>
                            <div class="inner-content">
                                <ul class="menu-01">
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h3 class="title"><i class="fa fa-cog" aria-hidden="true"></i>settings</h3>
                            <div class="inner-content">
                                <ul class="menu-01">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <?= $this->Flash->render() ?>
        <?php if($authUser) { ?>
        <?= $this->fetch('sidebar') ?>
        <?php } ?>
        <div class="" id="page-content-wrapper">
            <?= $this->fetch('content') ?>
        </div>
        <footer>
            <?= $this->fetch('footer') ?>
            <?= $this->fetch('script') ?>
            <?= $this->Html->script(['bootstrap/popper.min', 'bootstrap/bootstrap.min', 'common/common', 'common/calendar', 'dxdatagrid/jszip.min', 'dxdatagrid/dx.all', 'date-picker/moment.min', 'date-picker/daterangepicker.min', 'dal/dal', 'jquery/jquery-ui','report/report.data.source.js', 'report/data.grid.helper', 'report/columns_config_index', 'report/column.chooser.modal', 'report/report']); ?>
        </footer>
    </div>
</body>
</html>
