<?php
    $this->start('navbar');
        echo $this->element('navbar/navbar');
    $this->end(); 
    $this->start('sidebar');
        echo $this->element('sidebar/sidebar');
    $this->end(); 
?>
<div class="container-fluid row-light">
    <div class="row">
        <div class="col-md-5">
            <div class="inline-block-box">
                <div class="search-add-on">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <?=  $this->Form->text('search', ['class' => 'select-pareto-dsgn', 'type' => 'search', 'placeholder' => 'Search', 'aria-label' => 'Search']); ?>
                </div>
            </div>
            <div class="select-inline">
                <div class="filter-add-on">
                    <i class="fa fa-filter" aria-hidden="true"></i>
                    <?= $this->Form->select(
                        'field',
                        ['Filter 1', 'Filter 2', 'Filter 3', 'Filter 4', 'Filter 5'],
                        ['empty' => 'Filter', 'class' => 'select-pareto-dsgn', 'style' => 'width:150px;']
                    ); ?>
                </div>
            </div>
        </div>
        <div class="col-md-7 text-right right-section">
            <div class="select-inline">
                <div class="calender-add-on">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    <?=  $this->Form->control('date_range', ['label' => false, 'class' => 'select-pareto-dsgn datepicker-inpt report_filter','id' => 'date_range']); ?>
                </div>
            </div>
        </div>
    </div>
</div>

	<div class="row listing-type-row">
		<div class="filter-tab-row">
			<!-- \\\ Tabs links //// -->
			<div class="float-md-left">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active show" id="dimensions-tab" data-toggle="tab" href="#dimensions" role="tab" aria-controls="dimensions" aria-selected="true">Dimensions</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="campaigns-tab" data-toggle="tab" href="#campaigns" role="tab" aria-controls="campaigns" aria-selected="false">Campaigns</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="adset-tab" data-toggle="tab" href="#adset" role="tab" aria-controls="adset" aria-selected="false">Ad sets</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="ads-tab" data-toggle="tab" href="#ads" role="tab" aria-controls="ads" aria-selected="false">Ad sets</a>
					</li>
				</ul>
			</div>
			<div class="float-md-right">
				<div class="view-setting">
					<a href="#" class="text-uppercase viewas">View As:</a>
					<a href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
                    <a data-toggle="modal" href="#ColumnChooserModal" title="Column Chooser" onclick="initiateColumnChooserModal();"><i class="fa fa-align-justify" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="tab-content pareto-tab-content" id="pareto-content-container">
				<div class="tab-pane fade active show" id="dimensions" role="tabpanel" aria-labelledby="dimensions-tab">
					<div class="pareto-content">
						<div class="pareto-header">
							<div class="row">
								<div class="float-md-left">
                                    <div class="select-inline">
                                    
                                        <?= $this->Form->select(
                                            'report_type',
                                            ["by_date" => "By Date"],
                                            ['class' => 'select-pareto-dsgn', 'style' => 'width:134px;', 'default'=>'By Date', 'id'=>'report_type']
                                        ); ?>

									</div>
									<div class="select-inline">
										<div class="filter-add-on">
											<i class="fa fa-filter" aria-hidden="true"></i>
                                             <?= $this->Form->select('attribution_window', ['default'], ['id' => 'attribution_window', 'empty' => 'Attribution Window', 'class' => 'select-pareto-dsgn', 'style' => 'width:150px;', 'default'=>'default']); ?>
										</div>
									</div>
									<div class="select-inline">
                                        <?= $this->Form->select(
                                           'Group by',
                                           ['GEO Lng'],
                                           ['empty' => 'Group by', 'class' => 'select-pareto-dsgn', 'style' => 'width:190px;', 'default'=>'GEO Lng']
                                        ); ?>
									</div>

									<div class="select-inline">
                                        <?= $this->Form->select(
                                           'Audience',
                                           ['Acqisition'],
                                           ['empty' => 'Acqisition', 'class' => 'select-pareto-dsgn', 'style' => 'width:200px;', 'default'=>'Acqisition']
                                        ); ?>
									</div>
									<div class="select-inline">
                                         <?= $this->Form->select(
                                           'Columns',
                                           ['default'],
                                           ['class' => 'select-pareto-dsgn', 'style' => 'width:162px;', 'id' =>'column_selector']
                                        ); ?>
									</div>
								</div>
								<div class="float-md-right">
									<div class="links-col">
										<a href="chart.html" class="graph"></a>
										<a href="#" title="Export" id="export" class="export"></a>
									</div>
								</div>
							</div>
						</div>
						<div class="pareto-table">
                            <div id="loadPanelContainer"></div>
                            <div id="DimensionsTable"></div>
						</div>
					</div>
				</div>
                <div class="tab-pane fade" id="campaigns" role="tabpanel" aria-labelledby="adset-tab">
					<div class="pareto-content">
						<div class="pareto-header">
							<div class="row">
								<div class="float-md-left">
								</div>
								<div class="float-md-right">
									<div class="links-col">
									</div>
								</div>
							</div>
						</div>
						<div class="pareto-table">
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="adset" role="tabpanel" aria-labelledby="adset-tab">
					<div class="pareto-content">
						<div class="pareto-header">
							<div class="row">
								<div class="float-md-left">
								</div>
								<div class="float-md-right">
									<div class="links-col">
									</div>
								</div>
							</div>
						</div>
						<div class="pareto-table">
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="ads" role="tabpanel" aria-labelledby="ads-tab">
					<div class="pareto-content">
						<div class="pareto-header">
							<div class="row">
								<div class="float-md-left">
								</div>
								<div class="float-md-right">
									<div class="links-col">
									</div>
								</div>
							</div>
						</div>
						<div class="pareto-table">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- The Modal -->
    <div class="modal fade" id="ColumnChooserModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Customise Columns</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container coloumn_chooser">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group has-search">
                                    <input type="text" id="searchColumn" onkeyup="searchColumn()" class="form-control" placeholder="Search">
                                </div>
                            </div>
                            <div class="col-md-7" id="searchContainer">
                                <h4 class="modal-title">Available Columns</h4>
                                <ul class="columnContainer">
                                </ul>
                            </div>
                            <div class="col-md-5">
                                <h4 class="modal-title">Selected Columns</h4>
                                <ul class="ui-sortable" id="sortable">
                                <ul>
                            </div>
                        </div>
                    </div> 
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <div class="col-10 mr-auto">
                        <label><input class="save_preset mr-auto" type="checkbox" value="1" />  Save as preset</label>
                        <input type="text" id="view" required="required" class="save_view_type mr-auto" />
                        <div id="btnSave" class="save_view_type"></div>
                         <div class="invalid-feedback text-center">This name alreday exist!</div>
                    </div>
                    <div class="col-2 mr-auto">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php  $this->start('footer'); echo $this->element('footer/footer'); $this->end(); ?>