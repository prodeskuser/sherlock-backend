<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FacebookAdInsightsActions Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\CampaignsTable|\Cake\ORM\Association\BelongsTo $Campaigns
 * @property \App\Model\Table\AdsetsTable|\Cake\ORM\Association\BelongsTo $Adsets
 * @property \App\Model\Table\AdsTable|\Cake\ORM\Association\BelongsTo $Ads
 *
 * @method \App\Model\Entity\FacebookAdInsightsAction get($primaryKey, $options = [])
 * @method \App\Model\Entity\FacebookAdInsightsAction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsightsAction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsightsAction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FacebookAdInsightsAction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsightsAction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsightsAction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FacebookAdInsightsActionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('facebook_ad_insights_actions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public static function getDBColName($name, $postfix = "")
    {
        if (strpos($name, '.') !== false) {
            $parts = explode(".", $name);
            return $parts[1] . $postfix;
        }
        return $name;
    }

    public static function createInsightsHash($arr)
    {
        if (!isset ($arr['action_report_time']))
            $arr['action_report_time'] = "";
        $date_str = $arr['request_date']->format('Y-m-d');
        $str = $arr['ad_id'] . $date_str . $arr['attribution_window'] . $arr['action_report_time'];
        return hash('sha256', $str);
    }

    /**
     * same hash as used in insights table
     * @param $arr
     * @return string
     */
    public static function createRefIdHash ($arr)
    {
        $date_str = $arr['request_date']->format('Y-m-d');
        $str = $arr['ad_id'] . $date_str;
        return hash('sha256', $str);
    }

    //INSERT AND UPDATE Insights Actions
    public function saveActions($rows)
    {
        $data=[];
        foreach ($rows as $key=>&$row) {
            $id = FacebookAdInsightsActionsTable::createInsightsHash($row);
            $ref_id = FacebookAdInsightsActionsTable::createRefIdHash($row);
            $data[$key] = $row;
            $data[$key]['id'] = $id;
            $data[$key]['ref_id'] = $ref_id;
        }
        $entities = $this->newEntities($data);
        $result   = $this->saveMany($entities);
        return true;
    }
    
    //GET InsightsActions
    public function getInsightsActionsData($where){
        $query = $this->find()->where($where)->select(['id', 'request_date', 'attribution_window', 'account_id', 'account_name', 'campaign_id', 'campaign_name', 'adset_id', 'adset_name', 'ad_name', 'ad_id', 'fb_pixel_purchase', 'fb_pixel_purchase_value']);
        
        return  $query->toArray();
    }
}