<?php
namespace App\Model\Table;

use App\Model\Entity\FacebookAdsets;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use FacebookAds\Object\Fields\AdSetFields;

class FacebookAdsetsTable extends Table
{
	public function initialize(array $config) {

	}

    public function getAdset($account_id, $adset) {
        $adset = $this->find('all')->where(['account_id' => $account_id, 'adset_id' => $adset->id])->toArray();

        return $adset;
    }

    public function addAdset($adset, $user, $last_updated=null) {
        $data = ['account_id' => $adset->account_id, 'campaign_id' => $adset->campaign_id, 'adset_id' => $adset->id, 'name' => $adset->name, 'status' => $adset->status,'user_id'=>$user->user_id, 'budget'=>$adset->daily_budget, 'billing_event'=>$adset->billing_event, 'optimization_goal'=>$adset->optimization_goal, 'last_updated' => $last_updated];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateAdset($adset_data, $adset, $last_updated=null) {
        $id = $adset_data[0]['id'];
        $update = $this->get($id);
        $update->status = $adset->status;
        $update->last_updated = $last_updated;

        if($this->save($update)) {
            return true;
        } else {
            return false;
        }
    }

    public function setAccountStatus($status, $user_id, $last_updated) {
        $this->updateAll(["status" => $status], ["user_id" => $user_id, "last_updated < " => $last_updated]);
        return true;
    }
    
    public function getAdSetFields()
    {
        $fields = array(
            AdSetFields::ID,
            AdSetFields::NAME,
            AdSetFields::ACCOUNT_ID,
            AdSetFields::CAMPAIGN_ID,
            AdSetFields::BILLING_EVENT,
            AdSetFields::OPTIMIZATION_GOAL,
            AdSetFields::PROMOTED_OBJECT,
            AdSetFields::ATTRIBUTION_SPEC,
            AdSetFields::DAILY_BUDGET,
            AdSetFields::BUDGET_REMAINING,
            AdSetFields::LIFETIME_BUDGET,
            AdSetFields::CREATED_TIME,
            AdSetFields::UPDATED_TIME,
            AdSetFields::STATUS);
        return $fields;
    }
}

?>