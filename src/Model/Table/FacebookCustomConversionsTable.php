<?php

namespace App\Model\Table;

use App\Model\Entity\FacebookCustomConversions;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use FacebookAds\Object\Fields\CustomConversionFields;
use Cake\Datasource\ConnectionManager;

class FacebookCustomConversionsTable extends Table
{
	public function initialize(array $config) {
		$this->addBehavior("Timestamp");
	}

    public function getRecord($account_id, $custom_conversion_id) {
        $get_custom_conversions = $this->find('all')->where(['account' => $account_id, 'response_name' => $custom_conversion_id])->toArray();

        return $get_custom_conversions;
    }

    public function add($conversion, $count) {
        //echo "<pre>"; print_r($conversion); echo $conversion->account_id;

        $to_extract = 1;

        if($count > 5)
            $to_extract = 0;

        $data = ['account' => $conversion->account_id, 'response_name' => $conversion->id, 'internal_name' => 'custom'.$count, 'display_name' => 'custom_con_display_name', 'to_extract' => $to_extract];
        $save = $this->newEntity($data);
        $result = $this->save($save);

        $id = $result->id;

        return $id;
    }
    
    public static function getCustomCoversionDBColName($account_id, $name) {
        if (strpos($name, '.') !== false) {
            $parts = explode(".", $name);

            if($parts[1] == 'custom') {
                $connection = ConnectionManager::get('default');
                $row = $connection->execute("select * from facebook_custom_conversions where account = '$account_id' and response_name = ".$parts[2])->fetchAll('assoc');

                if(!empty($row)) {
                    if($row[0]['to_extract'] == 1) {
                        return $row[0]['internal_name'];
                    }
                }
            }

            //return $parts[1] . $postfix;*/
        }
    }

    public function getCustomConversionFields()
    {
        $fields =[
            CustomConversionFields::ACCOUNT_ID,
            CustomConversionFields::NAME,
            CustomConversionFields::ID
        ];
        return $fields;
    }
}

?>