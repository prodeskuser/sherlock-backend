<?php
namespace App\Model\Table;

use App\Model\Entity\FacebookCampaigns;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use FacebookAds\Object\Fields\CampaignFields;

class FacebookCampaignsTable extends Table
{
	public function initialize(array $config) {
		
	}

    public function getCampaign($account_id, $campaign) {
        $campaign = $this->find('all')->where(['account_id' => $account_id, 'campaign_id' => $campaign->id])->toArray();

        return $campaign;
    }

    public function addCampaign($account_id, $campaign, $user_data, $last_updated=null) {
        $data = ['account_id' => $account_id, 'campaign_id' => $campaign->id, 'name' => $campaign->name, 'objective' => $campaign->objective, 'status' => $campaign->status, 'user_id' => $user_data->user_id, 'created_time'=>$campaign->created_time, 'updated_time'=>$campaign->updated_time, 'start_time'=>$campaign->start_time, 'stop_time'=>$campaign->stop_time, 'buying_type'=>$campaign->buying_type, 'last_updated'=> $last_updated];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateCampaign($campaign_data, $campaign, $last_updated=null) {
        $id = $campaign_data[0]['id'];

        $update = $this->get($id);
        $update->status = $campaign->status;
        $update->last_updated = $last_updated;

        if($this->save($update)) {
            return true;
        } else {
            return false;
        }
    }

    public function setAccountStatus($status, $user_id, $last_updated) {
        $this->updateAll(["status" => $status], ["user_id" => $user_id, "last_updated < " => $last_updated]);

        return true;
    }
    
    public function getCampaignFields() 
    {
        $fields = array(
            CampaignFields::ID,
            CampaignFields::NAME,
            CampaignFields::ACCOUNT_ID,
            CampaignFields::OBJECTIVE,
            CampaignFields::BUYING_TYPE,
            CampaignFields::START_TIME,
            CampaignFields::STOP_TIME,
            CampaignFields::CREATED_TIME,
            CampaignFields::UPDATED_TIME,
            CampaignFields::STATUS
        );
        return $fields;
    }
}

?>