<?php

namespace App\Model\Table;

use App\Model\Entity\SnapAccounts;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SnapAdsTable extends Table
{
	public function initialize(array $config) {
		
	}

    public function addAdAccountCampaignAdSquadAd($user_id, $ad_account, $ad_account_campaign, $ad_account_campaign_ad_squad, $params = array(), $last_updated) {
        //echo "<pre>"; print_r($params); die;

        $data = ['user_id' => $user_id, 'ad_id' => $params['ad']['id'], 'ad_account_id' => $ad_account['adaccount']['id'], 'ad_account_name' => $ad_account['adaccount']['name'], 'ad_campaign_id' => $ad_account_campaign['campaign']['id'], 'ad_campaign_name' => $ad_account_campaign['campaign']['name'], 'ad_squad_id' => $ad_account_campaign_ad_squad['adsquad']['id'], 'ad_squad_name' => $ad_account_campaign_ad_squad['adsquad']['name'], 'creative_id' => $params['ad']['creative_id'], 'name' => $params['ad']['name'], 'type' => $params['ad']['type'], 'status' => $params['ad']['status'], 'last_updated' => $last_updated];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateAdAccountCampaignAdSquadAd($ad_account_campaign_ad_squad_ad_data, $ad_squad_ad, $ad_account_id, $campaign_id, $ad_squad_id, $last_updated) {
        //echo "<pre>"; print_r($last_updated);

        $id = $ad_account_campaign_ad_squad_ad_data[0]['id'];

        $ad_account_campaign_ad_squad_ad_update = $this->get($id);
        $ad_account_campaign_ad_squad_ad_update->status = $ad_squad_ad['ad']['status'];
        $ad_account_campaign_ad_squad_ad_update->last_updated = $last_updated;

        if($this->save($ad_account_campaign_ad_squad_ad_update)) {
            return true;
        } else {
            return false;
        }
    }

    public function UpdateAdAccountCampaignAdSquadAdStats($ad_account_campaign_ad_squad_ad_data, $ad_stat, $last_updated) {
        //echo "<pre>"; print_r($ad_stat);

        $id = $ad_account_campaign_ad_squad_ad_data[0]['id'];

        $ad_account_campaign_ad_squad_ad_update = $this->get($id);
        $ad_account_campaign_ad_squad_ad_update->impressions = $ad_stat['total_stat']['stats']['impressions'];
        //$ad_account_campaign_ad_squad_ad_update->clicks = $ad_stat['total_stat']['stats']['clicks'];
        $ad_account_campaign_ad_squad_ad_update->spend = $ad_stat['total_stat']['stats']['spend'];
        //$ad_account_campaign_ad_squad_ad_update->swipes = $ad_stat['total_stat']['stats']['swipes'];
        //$ad_account_campaign_ad_squad_ad_update->screen_time_millis = $['total_stat']['stats']['screen_time_millis'];
        $ad_account_campaign_ad_squad_ad_update->last_updated = $last_updated;

        if($this->save($ad_account_campaign_ad_squad_ad_update)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAdAccountCampaignAdSquadsAd($user_id , $ad_account_id, $campaign_id, $ad_squad_id, $ad_id) {
        $ad_account_campaign_ad_squad_ad_data = $this->find('all')->where(['user_id' => $user_id, 'ad_account_id' => $ad_account_id, 'ad_campaign_id' => $campaign_id, 'ad_squad_id' => $ad_squad_id, 'ad_id' => $ad_id])->toArray();

        return $ad_account_campaign_ad_squad_ad_data;
    }

    public function setAdAccountCampaignAdSquadAdDeleteStatus($user_id, $last_updated) {
        $this->updateAll(["is_deleted" => 1], ["user_id" => $user_id, "last_updated < " => $last_updated]);

        return true;
    }

}

?>