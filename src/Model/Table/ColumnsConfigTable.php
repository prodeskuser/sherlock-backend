<?php

namespace App\Model\Table;

//use App\Model\Entity\FacebookInsights;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class ColumnsConfigTable extends Table 
{
	public function initialize(array $config){
		$this->addBehavior("Timestamp");
	}

    public function getColumns($where, $orderBy=null){
        $columnsconfig = TableRegistry::get('columns_config');
        return $columnsconfig->find('all')->where($where)->order($orderBy);
    }

    public function saveColumn($data){
        $columnsconfigTable = TableRegistry::get('columns_config');
        $columns_config = $columnsconfigTable->newEntity();

        $columns_config->user_id    = $data['user_id'];
        $columns_config->platform   = $data['platform'];
        $columns_config->type       = $data['type'];
        $columns_config->db_name    = (isset($data['db_name']) ? $data['db_name'] : null); 
        $columns_config->user_name  = $data['user_name'];
        $columns_config->expression = (isset($data['expression']) ? $data['expression'] : null);

        if($columnsconfigTable->save($columns_config)) {
            return $columns_config->id;
        } 
    }

    public function updateColumns($oldData, $newData){
        $columnsconfigTable = TableRegistry::get('columns_config');
        $columns_config = $columnsconfigTable->get($oldData['id']); // Return with id 

        foreach($newData as $key => $val) {
            $columns_config->$key = $val;
        }
        if($columnsconfigTable->save($columns_config)) {
            return $columns_config->id;
        } 
    }

    public function removeColumn($data){
        $entity = $this->get($data['id']);
        if($this->delete($entity)){
            return $data['id'];
        }
    }
}
?>