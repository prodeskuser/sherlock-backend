<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use FacebookAds\Object\Fields\AdAccountFields;

class FacebookAdAccountsTable extends Table
{

    public function updateOrInsertMany($rows, $user_id)
    {
        return $this->updateOrInsertManyByField($rows, $user_id, 'account_id');
    }

    public function getOne($user_id, $account_id)
    {
        return $this->find('all')->where([
            'user_id' => $user_id,
            'account_id' => $account_id,
        ])->first();
    }

    public function getAccountIdsForDownload($user_id)
    {
        return $this->find('all')->where([
            'user_id' => $user_id,
            'download_data' => true
        ])->extract('account_id')->toArray();
    }

    public function getAdAccountsForDownload($user_id)
    {
        return $this->find('all')->where([
            'user_id' => $user_id,
            'download_data' => true,
            'is_enabled'=> true
        ])->toArray();
    }

    public function getAdAccounts($column, $where, $orderBy)
    {
        return $this->find()->select($column)->where($where)->order($orderBy)->toArray();
    }

    public function getAdAccountOptions($user_id, $filters = [])
    {
        $query = $this->find('list', [
                'keyField' => 'account_id',
                'valueField' => 'name'
            ])
            ->where(['user_id' => $user_id]);

        if (!empty($filters)) {
            $query->where($filters);
        }

        return $query->order(['name' => 'ASC'])
            ->toArray();
    }

    public function getAllByUserId($user_id)
    {
        return $this->find('all')->where(['user_id' => $user_id])->toArray();
    }
    
    public function getAccount($user, $account) {
        $account = $this->find('all')->where(['user_id' => $user->user->id, 'account_id' => $account->account_id])->toArray();

        return $account;
    }

    public function addAccount($user, $account) {
        $data = ['user_id' => $user->user_id, 'account_id' => $account->account_id, 'name' => $account->name, 'account_status' => $account->account_status,'download_data'=>0,'is_enabled'=>0, 'campaign_filter_string'=>null, 'attribution_window'=>'default'];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateAccount($account_data, $account, $last_updated=null) {
        //echo "<pre>"; print_r($last_updated);

        $id = $account_data[0]['id'];

        $account_update = $this->get($id);
        $account_update->status = $account->account_status;

        if($this->save($account_update)) {
            return true;
        } else {
            return false;
        }
    }

    public function setAccountStatus($user_id, $last_updated) {
        $this->updateAll(["is_deleted" => 1], ["user_id" => $user_id, "last_updated < " => $last_updated]);

        return true;
    }
    
    public function getAdAccountFields()
    {
        $fields = array(
            AdAccountFields::ID,
            AdAccountFields::NAME,
            AdAccountFields::ACCOUNT_ID,
            AdAccountFields::ACCOUNT_STATUS
        );
        return $fields;
    }

    public function getUserAccountData($user_id){
        $query  = $this
            ->find('all',array(
                    'conditions' => [
                        'FacebookAdAccounts.user_id'=>$user_id,
                        'FacebookAdAccounts.is_enabled'=>true,
                    ]
                )
            )
            ->join([
                'table' => 'report_views',
                'alias' => 'ReportViews',
                'type' => 'LEFT',
                'conditions' => [
                    'ReportViews.account_id = FacebookAdAccounts.account_id',
                ]
            ])
        ;
        $query->select(['FacebookAdAccounts.account_id', 'FacebookAdAccounts.name', 'FacebookAdAccounts.attribution_window', 'ReportViews.view_name']);
        $query->hydrate (false)->toArray();
        return $query;
    }
}
