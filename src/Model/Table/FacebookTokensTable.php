<?php
namespace App\Model\Table;

use App\Model\Entity\FacebookToken;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * FacebookTokens Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class FacebookTokensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('facebook_tokens');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('access_token');

        $validator
            ->add('email', 'valid', ['rule' => 'email'])
            ->allowEmpty('email');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }


    public function getTokenByUser($user_id)
    {

        $tokensQuery = $this->find()->where(['user_id' =>$user_id]);

        $tokens =$tokensQuery->toArray();

        if (!$tokens)
        {

            Log::write('debug', 'could not find token');
            return null;
        }


        Log::write('debug', 'token was found');
        $token = $tokens[0]->access_token;

        return $token;

    }

    /**
     * Returns all tokens.
     * @return array An array of tokens.
     */
    public function getAllTokens()
    {
        return $this->find('all')->contain(['Users'])->toArray();
    }

}
