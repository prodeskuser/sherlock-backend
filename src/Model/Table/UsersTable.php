<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Datasource\EntityInterface;

class UsersTable extends Table 
{
	public function initialize(array $config){
		$this->addBehavior("Timestamp");
		$this->hasMany('UsersAccounts', [
            'foreignKey' => 'user_id'
        ]);
	}
	public function validationDefault(Validator $validator){
		 $validator->requirePresence('password','You must enter your password')
					 ->add('password', [
						'length' => [
						'rule' => ['minLength', 4],
						'message' => 'Password need to be at least 4 characters long',
						]
					])->add('password', [
					'compare' => [
						'rule' => ['compareWith', 'password2'],
						'message'=>"Password mismatch password confirm!"
						]
					])
                    ->notEmpty('password2','Please enter confirm password')
                    ->notEmpty('full_name','Please enter full name')
					->notEmpty('email','Please enter your email!')
					->add('email', [
					'email' => [
						'rule' => ['email'],
						'message' => "Please enter valid email!"
						]
					]);

					return $validator;
	}
	    /**
     * Returns a rules checker object . It is used  for validating data
     * I used it her to check if email is unique 
	 **/
	
    public function beforeSave(\Cake\Event\Event $event, EntityInterface $entity) {
        if($entity->isNew()) {
            $entity->id = $this->generateId(30);
        }
    }

    public function generateId() {
        $randstr='';

        srand((double) microtime(TRUE) * 1000000);

        //our array add all letters and numbers if you wish
        $chars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5',
        '6', '7', '8', '9');

        $length = sizeof($chars);

        for($rand = 0; $rand <= $length; $rand++) {
            $random = rand(0, count($chars) - 1);
            $randstr .= $chars[$random];
        }

        return $randstr;
    }

	public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    public function getUser($params = array()) {
        return $this->find('all', array('conditions' => $params))->toArray();
    }

    public function updateSnapchatInfo($access_token, $refresh_token, $user_id) {
        $this->updateAll(["access_token" => $access_token, "refresh_token" => $refresh_token], ["id" => $user_id]);

        return true;
    }
}

?>