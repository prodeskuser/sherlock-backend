<?php

namespace App\Model\Table;

use App\Model\Entity\SnapAccounts;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SnapAccountsTable extends Table
{
	public function initialize(array $config) {
		
	}

    public function addAdAccount($user_id, $params = array(), $last_updated) {
        $data = ['user_id' => $user_id, 'organization_id' => $params['adaccount']['organization_id'], 'ad_account_id' => $params['adaccount']['id'], 'name' => $params['adaccount']['name'], 'type' => $params['adaccount']['type'], 'status' => $params['adaccount']['status'], 'currency' => $params['adaccount']['currency'], 'timezone' => $params['adaccount']['timezone'], 'advertiser_organization_id' => $params['adaccount']['advertiser_organization_id'], 'billing_center_id' => $params['adaccount']['billing_center_id'], 'billing_type' => $params['adaccount']['billing_type'], 'last_updated' => $last_updated];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateAdAccount($ad_account_data, $ad_account, $last_updated) {
        //echo "<pre>"; print_r($last_updated);

        $id = $ad_account_data[0]['id'];

        $ad_account_update = $this->get($id);
        $ad_account_update->status = $ad_account['adaccount']['status'];
        $ad_account_update->last_updated = $last_updated;

        if($this->save($ad_account_update)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAdAccount($user_id , $organization_id, $ad_account_id) {
        $ad_account_data = $this->find('all')->where(['user_id' => $user_id, 'organization_id' => $organization_id, 'ad_account_id' => $ad_account_id])->toArray();

        return $ad_account_data;
    }

    public function setAdAccountDeleteStatus($user_id, $last_updated) {
        $this->updateAll(["is_deleted" => 1], ["user_id" => $user_id, "last_updated < " => $last_updated]);

        return true;
    }
}

?>