<?php
namespace App\Model\Table;

use App\Model\Entity\FacebookAds;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use FacebookAds\Object\Fields\AdFields;

class FacebookAdsTable extends Table
{
	public function initialize(array $config) {
		
	}

    public function getAd($account_id, $campaign_id, $adset_id, $ad_id, $date) {
        $campaign = $this->find('all')->where(['account_id' => $account_id, 'campaign_id' => $campaign_id, 'adset_id' => $adset_id, 'ad_id' => $ad_id])->toArray();

        return $campaign;
    }

    public function addAd($ad, $date, $user, $last_update) {
        $data = ['account_id' => $ad->account_id, 'campaign_id' => $ad->campaign_id, 'adset_id' => $ad->adset_id, 'ad_id' => $ad->id, 'name' => $ad->name, 'status' => $ad->status, 'requested_date' => $date, 'bid_amount'=>(isset($ad->bid_amount) ? $ad->bid_amount : null), "creative_id"=>(isset($ad->creative) ? $ad->creative['id'] : null), 'created_time'=> $ad->created_time, 'updated_time'=>$ad->updated_time, 'user_id'=>$user->user_id, 'last_updated'=>$last_update];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateAd($ad_data, $ad, $last_update) {
        $id = $ad_data[0]['id'];

        $update = $this->get($id);
        $update->status = $ad->status;
        $update->last_updated = $last_update;

        if($this->save($update)) {
            return true;
        } else {
            return false;
        }
    }

    public function setAccountStatus($status, $user_id, $last_updated) {
        $this->updateAll(["status" => $status], ["user_id" => $user_id, "last_updated < " => $last_updated]);

        return true;
    }
    
    public function getAdFields()
    {
        $fields = array(
                    AdFields::ID,
                    AdFields::NAME,
                    AdFields::ACCOUNT_ID,
                    AdFields::BID_AMOUNT,
                    AdFields::BID_INFO,
                    AdFields::BID_TYPE,
                    AdFields::CAMPAIGN_ID,
                    AdFields::ADSET_ID,
                    AdFields::CONFIGURED_STATUS,
                    AdFields::EFFECTIVE_STATUS,
                    AdFields::CREATIVE,
                    AdFields::CREATED_TIME,
                    AdFields::UPDATED_TIME,
                    AdFields::STATUS,
                );
        return $fields;
    }
}

?>