<?php

namespace App\Model\Table;

use App\Model\Entity\SnapAccounts;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SnapCampaignsTable extends Table
{
	public function initialize(array $config) {
		
	}

    public function addAdAccountCampaign($user_id, $params = array(), $last_updated) {
        //echo "<pre>"; print_r($params);

        if(isset($params['campaign']['daily_budget_micro']))
            $daily_budget_micro = $params['campaign']['daily_budget_micro'];
        else
            $daily_budget_micro = null;

        $data = ['user_id' => $user_id, 'campaign_id' => $params['campaign']['id'], 'ad_account_id' => $params['campaign']['ad_account_id'], 'name' => $params['campaign']['name'], 'objective' => $params['campaign']['objective'], 'daily_budget_micro' => $daily_budget_micro, 'start_time' => $params['campaign']['start_time'], 'status' => $params['campaign']['status'], 'last_updated' => $last_updated];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateAdAccountCampaign($ad_account_campaign_data, $ad_account_campaign, $last_updated) {
        //echo "<pre>"; print_r($last_updated);

        $id = $ad_account_campaign_data[0]['id'];

        $ad_account_campaign_update = $this->get($id);
        $ad_account_campaign_update->status = $ad_account_campaign['campaign']['status'];
        $ad_account_campaign_update->last_updated = $last_updated;

        if($this->save($ad_account_campaign_update)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAdAccountCampaign($user_id , $campaign_id, $ad_account_id) {
        $ad_account_campaign_data = $this->find('all')->where(['user_id' => $user_id, 'campaign_id' => $campaign_id, 'ad_account_id' => $ad_account_id])->toArray();

        return $ad_account_campaign_data;
    }

    public function setAdAccountCampaignDeleteStatus($user_id, $last_updated) {
        $this->updateAll(["is_deleted" => 1], ["user_id" => $user_id, "last_updated < " => $last_updated]);

        return true;
    }

}

?>