<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use FacebookAds\Object\Fields\AdsInsightsFields;
use Cake\ORM\TableRegistry;


/**
 * FacebookAdInsights Model
 *
 * @property \App\Model\Table\AdsTable|\Cake\ORM\Association\BelongsTo $Ads
 * @property \App\Model\Table\AdAccountsTable|\Cake\ORM\Association\BelongsTo $AdAccounts
 * @property \App\Model\Table\CampaignsTable|\Cake\ORM\Association\BelongsTo $Campaigns
 * @property \App\Model\Table\AdsetsTable|\Cake\ORM\Association\BelongsTo $Adsets
 *
 * @method \App\Model\Entity\FacebookAdInsight get($primaryKey, $options = [])
 * @method \App\Model\Entity\FacebookAdInsight newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsight[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsight|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FacebookAdInsight patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsight[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FacebookAdInsight findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FacebookAdInsightsTable extends Table
{

    PUBLIC $INSIGHTS_FIELDS_STRING = [
        'id',
        'user_id',
        'request_date',
        'ad_id',
        'account_id',
        'account_name',
        'campaign_id',
        'campaign_name',
        'adset_id',
        'adset_name',
        'ad_name',
        'status',
        'date_stop',
        'date_start',
        'created'
    ];
    PUBLIC $INSIGHTS_FIELDS_NUMBER = [
        'clicks',
        'frequency',
        'impressions',
        'reach',
        'spend',
        'inline_link_clicks',
        'relevance_score',
        'video_10_sec_watched_actions',
        'video_15_sec_watched_actions',
        'video_30_sec_watched_actions',
        'video_avg_percent_watched_actions',
        'video_complete_watched_actions',
        'video_p25_watched_actions',
        'video_p50_watched_actions',
        'video_p75_watched_actions',
        'video_p95_watched_actions',
        'video_p100_watched_actions',
        'external1',
        'external2',
        'external3',
        'external4',
        'external5',
    ];

    public $INSIGHTS_ACTIONS_FIELDS_NUMBER =[
        'fb_pixel_view_content',
        'fb_pixel_view_content_value',
        'fb_pixel_search',
        'fb_pixel_search_value',
        'fb_pixel_add_to_cart',
        'fb_pixel_add_to_cart_value',
        'fb_pixel_add_to_wishlist',
        'fb_pixel_add_to_wishlist_value',
        'fb_pixel_initiate_checkout',
        'fb_pixel_initiate_checkout_value',
        'fb_pixel_add_payment_info',
        'fb_pixel_add_payment_info_value',
        'fb_pixel_purchase',
        'fb_pixel_purchase_value',
        'fb_pixel_lead',
        'fb_pixel_lead_value',
        'fb_pixel_complete_registration',
        'fb_pixel_complete_registration_value',
        'fb_mobile_level_achieved',
        'fb_mobile_level_achieved_value',
        'fb_mobile_activate_app',
        'fb_mobile_activate_app_value',
        'fb_mobile_add_payment_info',
        'fb_mobile_add_payment_info_value',
        'fb_mobile_add_to_cart',
        'fb_mobile_add_to_cart_value',
        'fb_mobile_add_to_wishlist',
        'fb_mobile_add_to_wishlist_value',
        'fb_mobile_complete_registration',
        'fb_mobile_complete_registration_value',
        'fb_mobile_tutorial_completion',
        'fb_mobile_tutorial_completion_value',
        'fb_mobile_rate',
        'fb_mobile_rate_value',
        'fb_mobile_search',
        'fb_mobile_search_value',
        'fb_mobile_spent_credits',
        'fb_mobile_spent_credits_value',
        'fb_mobile_achievement_unlocked',
        'fb_mobile_achievement_unlocked_value',
        'fb_mobile_content_view',
        'fb_mobile_content_view_value',
        'custom1',
        'custom2',
        'custom3',
        'custom4',
        'custom5',
        'fb_mobile_purchase',
        'fb_mobile_purchase_value',
        'mobile_app_install',
    ];

    public $INSIGHTS_ACTIONS_FIELDS_STRING =[
        'id',
        'request_date',
        'attribution_window',
        'action_report_time',
        'account_id',
        'account_name',
        'campaign_id',
        'campaign_name',
        'adset_id',
        'adset_name',
        'ad_name',
        'ad_id',
        'date_start',
        'date_stop',
        'status',
        'created',
        'ref_id'];


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('facebook_ad_insights');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public static function createInsightsHash ($arr)
    {
        $str = $arr['ad_id'] . $arr['request_date_str'];
        return hash('sha256', $str);
    }


    public static function getInsightsFields()
    {
        $video_fields =[
            AdsInsightsFields::VIDEO_10_SEC_WATCHED_ACTIONS,
            //AdsInsightsFields::VIDEO_15_SEC_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_30_SEC_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_AVG_PERCENT_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_AVG_TIME_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_P100_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_P25_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_P50_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_P75_WATCHED_ACTIONS,
            AdsInsightsFields::VIDEO_P95_WATCHED_ACTIONS,
        ];

        $fields = [
            AdsInsightsFields::AD_NAME,
            AdsInsightsFields::AD_ID,
            AdsInsightsFields::IMPRESSIONS,
            AdsInsightsFields::CLICKS,
            AdsInsightsFields::REACH,
            AdsInsightsFields::FREQUENCY,
            AdsInsightsFields::SPEND,
            AdsInsightsFields::ACTIONS,
            AdsInsightsFields::ACTION_VALUES,
            AdsInsightsFields::ADSET_ID,
            AdsInsightsFields::CAMPAIGN_ID,
            AdsInsightsFields::ACCOUNT_ID,
            AdsInsightsFields::RELEVANCE_SCORE,
            AdsInsightsFields::UNIQUE_CLICKS,
            AdsInsightsFields::ACCOUNT_NAME,
            AdsInsightsFields::ADSET_NAME,
            AdsInsightsFields::CAMPAIGN_NAME,
            AdsInsightsFields::INLINE_LINK_CLICKS,
        ];
        return array_merge($video_fields,$fields);
    }

    //INSERT AND UPDATE Insights
    public function saveInsights ($rows)
    {
        $data=[];
        foreach ($rows as $key=>&$row) {
            $id = FacebookAdInsightsTable::createInsightsHash($row);
            $data[$key] = $row;
            $data[$key]['id'] = $id;
        }
        $entities = $this->newEntities($data);
        $result   = $this->saveMany($entities);
        return true;
    }

    // Get Insights
    public function getInsightsData($where){
        $query = $this->find("all")->where($where)->select(['id','clicks', 'impressions', 'spend', 'inline_link_clicks', 'ad_id', 'account_id', 'campaign_id', 'adset_id', 'campaign_name','request_date', 'date_start']);
        return  $query->toArray();
    }

    public function getAccounts($where){
        $query = $this->find('all')->where($where)->select(['account_id', 'account_name'])->distinct(['account_name']);

        return  $query->toArray();
    }

    public function getRecordInsightsWithAction($where, $sort, $groupby){
        $query  = $this->find('all',array('conditions' => $where,'order' => $sort, 'group' => $groupby))
            ->join([
                'table' => 'facebook_ad_insights_actions',
                'alias' => 'FacebookAdInsightsActions',
                'type' => 'INNER',
                'conditions' => 'FacebookAdInsightsActions.request_date = FacebookAdInsights.request_date AND FacebookAdInsightsActions.ad_id = FacebookAdInsights.ad_id'
            ]);
        $query = $query->select(['spend' => $query->func()->sum('spend'),'clicks' => $query->func()->sum('clicks'),'frequency' => $query->func()->sum('frequency'),'reach' => $query->func()->sum('reach'), 'impressions' => $query->func()->sum('impressions'), 'id', 'user_id', 'request_date', 'account_id', 'account_name', 'campaign_id', 'campaign_name', 'adset_id', 'adset_name', 'ad_name', 'inline_link_clicks', 'relevance_score', 'video_10_sec_watched_actions', 'video_15_sec_watched_actions', 'video_30_sec_watched_actions', 'video_avg_percent_watched_actions', 'video_complete_watched_actions', 'video_p25_watched_actions', 'video_p50_watched_actions', 'video_p75_watched_actions', 'video_p95_watched_actions', 'video_p100_watched_actions', 'external1', 'external2', 'external3', 'external4', 'external5', 'created', 'request_date', 'date_start', 'date_stop', 'FacebookAdInsightsActions.fb_pixel_purchase']);

        //debug($query);
        return $query;
    }

    public function byDate ($account_id,$from,$to,$filter,$attr_window='default')
    {
        $query  = $this
            ->find('all',array(
                    'conditions' => [
                        'FacebookAdInsights.account_id'=>$account_id,
                        'FacebookAdInsights.request_date >='=> $from,
                        'FacebookAdInsights.request_date <='=> $to,
                        'FacebookAdInsightsActions.request_date >='=> $from,
                        'FacebookAdInsightsActions.request_date <='=> $to,
                        'attribution_window'=>$attr_window
                    ],

                    'group' => ['FacebookAdInsights.request_date']
                )
            )
            ->join([
                'table' => 'facebook_ad_insights_actions',
                'alias' => 'FacebookAdInsightsActions',
                'type' => 'LEFT',
                'conditions' => [
                    'FacebookAdInsightsActions.ref_id = FacebookAdInsights.id',

                ]
            ])
        ;

        $select_array=[];
        $select_array['request_date']='date(FacebookAdInsights.request_date)';
        $select_array += $this->buildSelectArray($query,$this->INSIGHTS_FIELDS_NUMBER);
        $select_array += $this->buildSelectArray($query,$this->INSIGHTS_ACTIONS_FIELDS_NUMBER);

        $query->select($select_array);
        //debug ($query);

        $query->hydrate (false)->toArray();
        return $query;

    }

    /**
     * Returns a cakephp select array with sql aggregate func from array of columns names
     * @param $query
     * @param $arr
     */

    public function buildSelectArray (&$query,$columns,$op='sum')
    {
        $select_arr =[];
        foreach ($columns as $col)
        {
            $select_arr[$col]= $query->func()->coalesce ([$query->func()->{$op}($col),0]);
        }
        return $select_arr;
    }



}




