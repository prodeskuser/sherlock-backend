<?php

namespace App\Model\Table;

use App\Model\Entity\SnapAccounts;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SnapAdSquadsTable extends Table
{
	public function initialize(array $config) {
		
	}

    public function addAdAccountCampaignAdSquad($user_id, $ad_account_id, $params = array(), $last_updated) {
        //echo "<pre>"; print_r($params); die;

        if(isset($params['adsquad']['start_time']))
            $start_time = $params['adsquad']['start_time'];
        else
            $start_time = null;

        if(isset($params['adsquad']['bid_micro']))
            $bid_micro = $params['adsquad']['bid_micro'];
        else
            $bid_micro = null;

        if(isset($params['adsquad']['daily_budget_micro']))
            $daily_budget_micro = $params['adsquad']['daily_budget_micro'];
        else
            $daily_budget_micro = null;

        $data = ['user_id' => $user_id, 'ad_squad_id' => $params['adsquad']['id'], 'campaign_id' => $params['adsquad']['campaign_id'], 'ad_account_id' => $ad_account_id, 'name' => $params['adsquad']['name'], 'type' => $params['adsquad']['type'], 'placement' => $params['adsquad']['placement'], 'billing_event' => $params['adsquad']['billing_event'], 'bid_micro' => $bid_micro, 'daily_budget_micro' => $daily_budget_micro, 'start_time' => $start_time, 'optimization_goal' => $params['adsquad']['optimization_goal'], 'delivery_constraint' => $params['adsquad']['delivery_constraint'], 'status' => $params['adsquad']['status'], 'last_updated' => $last_updated];
        $save = $this->newEntity($data);
        $this->save($save);

        return true;
    }

    public function UpdateAdAccountCampaignAdSquad($ad_account_campaign_ad_squad_data, $ad_account_campaign_ad_squad, $last_updated) {
        //echo "<pre>"; print_r($last_updated);

        $id = $ad_account_campaign_ad_squad_data[0]['id'];

        $ad_account_campaign_ad_squad_update = $this->get($id);
        $ad_account_campaign_ad_squad_update->status = $ad_account_campaign_ad_squad['adsquad']['status'];
        $ad_account_campaign_ad_squad_update->last_updated = $last_updated;

        if($this->save($ad_account_campaign_ad_squad_update)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAdAccountCampaignAdSquads($user_id , $campaign_id, $ad_account_id, $ad_squad_id) {
        $ad_account_campaign_ad_squad_data = $this->find('all')->where(['user_id' => $user_id, 'campaign_id' => $campaign_id, 'ad_account_id' => $ad_account_id, 'ad_squad_id' => $ad_squad_id])->toArray();

        return $ad_account_campaign_ad_squad_data;
    }

    public function setAdAccountCampaignAdSquadDeleteStatus($user_id, $last_updated) {
        $this->updateAll(["is_deleted" => 1], ["user_id" => $user_id, "last_updated < " => $last_updated]);

        return true;
    }
}

?>