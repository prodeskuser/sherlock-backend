<?php

namespace App\Model\Table;
use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Hash;

class ReportViewsTable extends Table 
{

    //Get report by user_view
    public function getView($where) {
        return $this->find('all')->where($where)->toArray();
    }

    //Get user views
    public function getUserViews($where) {
        $a = $this->find('all')->where($where)->toArray();
        $result = Hash::combine($a, '{n}.view_name', '{n}.columns');
        return $result;
    }

    //Save report by user_view
    public function saveView($request, $user_id) {
        $data = ['user_id' => $user_id, 'account_id' => $request['account_id'], 'view_name' => $request['view_name'], 'columns' => $request['columns']];
        $save = $this->newEntity($data);
        if ($this->save($save)){
           return $id = $save->id;
        };
    }
}