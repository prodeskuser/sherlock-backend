<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class UsersController extends AppController {
    public function initialize()
    {
        parent::initialize();
		$this->loadComponent('Flash'); // Include the FlashComponent
		// Auth component allow visitors to access add action to register  and access logout action 
		$this->Auth->allow(['logout', 'add', 'login', 'thanks']);
    }

	public function index()
	{
        $user = $this->Users->newEntity();
		if ($this->request->is('post')) {
			// Auth component identify if sent user data belongs to a user
			$user = $this->Auth->identify();
			if ($user) {
				//
				$this->Auth->setUser($user);
				//return $this->redirect($this->Auth->redirectUrl());
                return $this->redirect('/reports');
			}
			$this->Flash->error(__('Invalid username or password, try again.'));
		}
        $this->set('user',$user);
	}

	public function logout(){
		$this->Flash->success('You have successfully logged out');	
        return $this->redirect($this->Auth->logout());
	}

	public function add()
	{
        $usersTable = TableRegistry::get('users');
        $user = $usersTable->newEntity();

		if($this->request->is('post')) {
			$this->Users->patchEntity($user, $this->request->data);

            $user->full_name = $this->request->data('full_name');
            $user->email = $this->request->data('email');
            $user->password = $this->request->data('password');

			if($usersTable->save($user)) {
                $this->loadModel('facebook_ad_insights');
                $facebook_insights_cols = $this->facebook_ad_insights->schema()->columns(); // Get columns of facebook_insight table

                $columns_config_table = TableRegistry::get('columns_config');

                foreach($facebook_insights_cols as $facebook_insights_col) {
                    $columns_config = $columns_config_table->newEntity();
                    $columns_config->user_id = $user->id;
                    $columns_config->db_name = $facebook_insights_col;
                    $columns_config->user_name = ucfirst(str_replace('_', ' ', $facebook_insights_col));

                    if($facebook_insights_col == 'impressions' || $facebook_insights_col == 'clicks' || $facebook_insights_col == 'reach' || $facebook_insights_col == 'frequency' || $facebook_insights_col == 'spend')
                        $columns_config->show = 1;

                    $columns_config_table->save($columns_config);
                }

                $columns_config = $columns_config_table->newEntity();
                $columns_config->user_id = $user->id;
                $columns_config->type = 'Custom';
                $columns_config->db_name = null;
                $columns_config->user_name = 'CTR';
                $columns_config->expression = 'clicks/impressions';
                $columns_config->show = 1;
                $columns_config_table->save($columns_config);

                $columns_config = $columns_config_table->newEntity();
                $columns_config->user_id = $user->id;
                $columns_config->type = 'Custom';
                $columns_config->db_name = null;
                $columns_config->user_name = 'CPM';
                $columns_config->expression = '1000*spend/impressions';
                $columns_config->show = 1;
                $columns_config_table->save($columns_config);

                return $this->redirect(['action' => 'thanks']);
			}

			$this->Flash->error(__('Unable to register your account.'));
		}

		$this->set('user',$user);
	}

    public function thanks() {
        
    }
}

?>