<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

use App\Lib\Facebook;

class FacebookController extends AppController {
    public function initialize() {
        parent::initialize();
		$this->loadComponent('Flash');

		$this->Auth->allow(['index', 'getAllCampaigns', 'getDownloadingInfo', 'fbutils']);

        $this->loadModel('Users');
        $this->loadModel('UsersAccounts');
        $this->loadModel('FacebookInsights');

        $this->obj = new Facebook;
        $this->obj->setAccessToken();

        ///if(!$this->Auth->user())
            //return $this->redirect('/users');
    }

	public function index() {
         $this->viewBuilder()->autoLayout(false);
	}
    
    
    public function fbutils(){

        /*----- Create Adset------*/
        if(isset($_POST['adsetForm'])){
            $expiration_time = date("Y-m-d h:i:s a",strtotime($_POST['expiration_time']));
            $craeteOfferParams = array(
                "location_type" =>$_POST['location_type'],
                "discounts" =>  json_encode(array(array("type"=>"bogo", "override"=>$_POST['description']))),
                "redemption_link"=> $_POST['redemption_link'],
                "redemption_code"=> $_POST['redemption_code'],
                "details"=> $_POST['details'],
                "expiration_time"=> (new \DateTime("$expiration_time"))->getTimestamp(),
                "access_token"=> $_POST['access_token']
            );

            $adurl = "https://graph.facebook.com/v3.0/".$_POST['page_id']."/nativeoffers";
            $offer_response = $this->obj->createApiCall("POST", $craeteOfferParams, $adurl);
            $offer_response = json_decode($offer_response);
            if(isset($offer_response->error->message)){
                echo json_encode(array("Error"=>$offer_response->error->message, "data"=>$offer_response->error));
                exit();
            }
            $adsetParams = array(
                "name"=>$_POST['name'],
                "lifetime_budget"=>$_POST['lifetime_budget'],
                "start_time"=>$_POST['start_time'],
                "end_time"=>$_POST['end_time'],
                "campaign_id"=>$_POST['campaign_id'],
                "billing_event"=>$_POST['billing_event'],
                "optimization_goal"=>$_POST['optimization_goal'],
                "bid_amount"=>(int)$_POST['bid_amount'],
                "promoted_object"=>json_encode(array("application_id"=>$_POST['application_id'],"offer_id"=>$offer_response->id, "object_store_url"=>$_POST['redemption_link'])), 
                "targeting" => '{"geo_locations":{"countries":["IL"]}, "user_device":["Android_Tablet","Android_Smartphone"], "user_os":["Android"]}',
                "access_token"=>$_POST['access_token']
            );
            $adseturl = "https://graph.facebook.com/v3.0/act_".$_POST['account_id']."/adsets";
            $adset_response  = $this->obj->createApiCall("POST", $adsetParams, $adseturl);
            $adset_response = json_decode($adset_response);
            echo  (isset($adset_response->error->message) ? json_encode(array("Error"=>$adset_response->error->message , "data"=>$adset_response)) : json_encode(array("success"=>"Ad Set Created Successfully", "offer_id"=>$offer_response->id, "data"=>$adset_response))); 
            exit();        
        }

        /*----- Create Duplicate Ad ------*/
        if(isset($_POST['adForm'])){ 
             /*------Get existing creative id------*/
            $url = "https://graph.facebook.com/v3.0/".$_POST['ad_id']."?fields=name,account_id,creative&access_token=".$_POST['access_token']."";
            $ad_data = $this->obj->createApiCall("GET", "", $url);
            $ad_data = json_decode($ad_data);
            if(!isset($ad_data->creative->id)){
                echo json_encode(array("Error"=>$ad_data->error->message , "data"=>$ad_data));
                exit();
            }

            $adParams = array("name"=>$ad_data->name, "adset_id"=>$_POST['adset_id'],"creative"=>json_encode(array("creative_id"=>$ad_data->creative->id)), "status"=>"PAUSED", "access_token"=>$_POST['access_token']);
            $adurl = "https://graph.facebook.com/v3.0/act_".$ad_data->account_id ."/ads";
            $ad_response = $this->obj->createApiCall("POST", $adParams, $adurl);
            $ad_response = json_decode($ad_response);

            echo (isset($ad_response->error->message) ? json_encode(array("Error"=>$ad_response->error->message , "data"=>$ad_response)) : json_encode(array("success"=>"Ad Created Successfully", "data"=>$ad_response))); 
            exit();
        }

        /*----- Create Duplicate Ad Set------*/
        if(isset($_POST['adsetcopiesForm'])){ 
            $expiration_time = date("Y-m-d h:i:s a",strtotime($_POST['expiration_time']));

            /*------Get existing Ad Set Details------*/
            $url = "https://graph.facebook.com/v3.0/".$_POST['ad_set_id']."?fields=id,account_id,name,lifetime_budget,start_time,end_time,campaign_id,billing_event,optimization_goal,bid_amount,promoted_object,targeting,configured_status,daily_budget,effective_status,bid_info,bid_strategy,destination_type&access_token=".$_POST['access_token']."";
            $adset_data = $this->obj->createApiCall("GET", "", $url);
             $adset_data = json_decode($adset_data);
            if(isset($adset_data->error->message)){
                echo json_encode(array("Get Adset Error"=>$adset_data->error->message , "data"=>$adset_data));
                exit();
            }

            /*--- Create New Offer ---*/
            $craeteOfferParams = array(
                "location_type" =>$_POST['location_type'],
                "discounts" =>  json_encode(array(array("type"=>"bogo", "override"=>$_POST['description']))),
                "redemption_link"=> $adset_data->promoted_object->object_store_url,
                "redemption_code"=> $_POST['redemption_code'],
                "details"=> $_POST['details'],
                "expiration_time"=> (new \DateTime("$expiration_time"))->getTimestamp(),
                "access_token"=> $_POST['access_token']
            );

            $adurl = "https://graph.facebook.com/v3.0/".$_POST['page_id']."/nativeoffers";
            $offer_response = $this->obj->createApiCall("POST", $craeteOfferParams, $adurl);
            $offer_response = json_decode($offer_response);
            if(isset($offer_response->error->message)){
                echo json_encode(array("Offer Error"=>$offer_response->error->message, "data"=>$offer_response->error));
                exit();
            }

                //Set Ad set Params
                $adsetParams = array(
                    "name"=>$_POST['adset_name'],
                    "campaign_id"=>$_POST['campaign_id'],
                    "billing_event"=>$adset_data->billing_event,
                    "optimization_goal"=>$adset_data->optimization_goal,
            "promoted_object"=>(isset($adset_data->promoted_object->custom_event_type) ? json_encode(array("page_id"=>$_POST['page_id'],"application_id"=>$adset_data->promoted_object->application_id, "object_store_url"=>$adset_data->promoted_object->object_store_url,"custom_event_type"=>$adset_data->promoted_object->custom_event_type, "offer_id"=>$offer_response->id)) : json_encode(array("page_id"=>$_POST['page_id'], "application_id"=>$adset_data->promoted_object->application_id, "object_store_url"=>$adset_data->promoted_object->object_store_url, "offer_id"=>$offer_response->id))),

                    "targeting" =>json_encode($adset_data->targeting),
                    "configured_status"=>$adset_data->configured_status,
                    "effective_status"=>$adset_data->effective_status,
                    "access_token"=>$_POST['access_token']
                );

            //Add Conditinol Params
            if(isset($adset_data->daily_budget)) {
                $adsetParams["daily_budget"] = $adset_data->daily_budget;
            }
            if(isset($adset_data->lifetime_budget)) {
                $adsetParams["lifetime_budget"] = $adset_data->lifetime_budget;
            }
            if(isset($adset_data->bid_amount)) {
                $adsetParams["bid_amount"] = $adset_data->bid_amount;
            }
            if(isset($adset_data->start_time)) {
                $adsetParams["start_time"] = $adset_data->start_time;
            }
            if(isset($adset_data->end_time)) {
                $adsetParams["end_time"] = $adset_data->end_time;
            }

            $adseturl = "https://graph.facebook.com/v3.0/act_".$adset_data->account_id ."/adsets";
            $adset_res  = $this->obj->createApiCall("POST", $adsetParams, $adseturl);
            $adset_res = json_decode($adset_res);
            if(isset($adset_res->error->message)){
                echo json_encode(array("Error"=>$adset_res->error->message , "data"=>$adset_res));
                exit();
            }
            if(!isset($_POST['duplicate_ads'])){
                echo json_encode(array("success"=>"Ad Set Duplicated Successfully", "data"=>array("offer_id"=>$offer_response->id, "adset_id"=>$adset_res->id)));
                exit();
            }
        
            /* Get All ads from existing adset ---*/
            $url= "https://graph.facebook.com/v3.0/".$_POST['ad_set_id']."/ads?fields=name,ad_specs,creative,status,adset_id,effective_status,account_id,image_hash,message&access_token=".$_POST['access_token']."";
            $ads_data  = $this->obj->createApiCall("GET", '', $url);

            $ads_data = json_decode($ads_data);

            if(isset($ads_data->error->message)){
                echo json_encode(array("Error"=>$adset_res->error->message , "data"=>$adset_res));
                exit();
            }

            $temp_ad=[];
            foreach($ads_data->data as $key=>$ad){
                $creativeurl = "https://graph.facebook.com/v3.0/".$ad->creative->id ."?fields=id,name,object_story_spec&access_token=".$_POST['access_token']."";
                $creative_res  = $this->obj->createApiCall("GET", '', $creativeurl);
                sleep(1);
                $creative_res = json_decode($creative_res);
                $creative_data  =   json_encode(array(
                                        "object_story_spec"=>array(
                                            "page_id"=>$creative_res->object_story_spec->page_id,
                                                "link_data" => array(
                                                    "offer_id"=>$offer_response->id,
                                                    "link"=> $creative_res->object_story_spec->link_data->link,
                                                    "message"=>(isset($creative_res->object_story_spec->link_data->message) ? $creative_res->object_story_spec->link_data->message : ''),
                                                    "name"=>(isset($creative_res->object_story_spec->link_data->name) ? $creative_res->object_story_spec->link_data->name : ''),
                                                    "image_hash"=>$creative_res->object_story_spec->link_data->image_hash
                                                )
                                        )
                                    ));

                //Set ad params
                $adParams = array("name"=>$ad->name, "adset_id"=>$adset_res->id,"creative"=>$creative_data, "status"=>$ad->status, "access_token"=>$_POST['access_token']);

                $adurl = "https://graph.facebook.com/v3.0/act_".$ad->account_id ."/ads";
                $ad_response = $this->obj->createApiCall("POST", $adParams, $adurl);
                $ad_response = json_decode($ad_response);

                if(isset($ad_response->error->message)){
                    echo json_encode(array("Error"=>$ad_response->error->message , "data"=>$ad_response));
                    break;
                    exit();
                }
                $temp_ad[$key]['id'] =  $ad_response->id;
            }

            echo json_encode(array("success"=>"Ad Set Duplicated Successfully", "Ad Ids "=>$temp_ad, "offer_id"=>$offer_response->id, "adset_id"=>$adset_res->id)); 
            exit();
        }
    }

    public function getAllCampaigns($ad_account_id=10405023) {
        $this->autoRender = false;

        $ad_account_campaigns = $this->obj->getAdAccountCampaigns($ad_account_id);

        //echo "<pre>"; print_r(json_decode($ad_account_campaigns)); die;

        $this->response->type('json');
        $this->response->body($ad_account_campaigns);
        return $this->response;
    }

    public function getDownloadingInfo() {
        $downloading_info = array();

        $ad_accounts = $this->UsersAccounts->getUserAccounts();

        //echo "<pre>"; print_r($ad_accounts); die;

        if(!empty($ad_accounts)) {
            foreach($ad_accounts as $key => $ad_account) {
                $ad_account_id = $ad_account->account_id;
                $ad_attribution_window = $ad_account->attribution_window;

                $downloading_info['adaccounts'][] = $ad_account;

                $ad_account_campaigns = json_decode($this->getAllCampaigns($ad_account_id));

                //echo "<pre>"; print_r($ad_account_campaigns);

                foreach($ad_account_campaigns->data as $ad_account_campaign) {
                    $campaign_id = $ad_account_campaign->id;

                    $downloading_info['adaccounts'][$ad_account_id]['campaigns'][] = $ad_account_campaign;

                    $ad_campaign_ads = json_decode($this->obj->getCampaignAds($campaign_id));

                    //echo "<pre>"; print_r($ad_campaign_ads);

                    foreach($ad_campaign_ads->data as $ad_campaign_ad) {
                        $ad_id = $ad_campaign_ad->id;
                        $adset_id = $ad_campaign_ad->adset_id;
                        $start_date = date('2018-01-01');
                        $end_date = date('2018-04-24');

                        $ad_account_campaign_ad_data = $this->FacebookInsights->getAdAccountCampaignAd($ad_account_id, $campaign_id, $adset_id, $ad_id);

                        //echo "<pre>"; print_r($ad_account_campaign_ad_data);

                        if(!empty($ad_account_campaign_ad_data)) {
                            $this->FacebookInsights->UpdateAdAccountCampaignAd($ad_account_id, $campaign_id, $adset_id, $ad_campaign_ad, $ad_account_campaign_ad_data);

                            $id = $ad_account_campaign_ad_data[0]->id;
                        } else {
                            $result = $this->FacebookInsights->addAdAccountCampaignAd($ad_account_id, $campaign_id, $adset_id, $ad_campaign_ad);

                            $id = $result;
                        }

                        $downloading_info['adaccounts'][$ad_account_id]['campaigns'][$campaign_id]['ads'][] = $ad_campaign_ad;

                        $get_account_campaign_ad_insights = json_decode($this->obj->getAdInsights($ad_id, $start_date, $end_date, $ad_attribution_window));

                        //echo "<pre>"; print_r($get_account_campaign_ad_insights); die;

                        if(!empty($get_account_campaign_ad_insights)) {
                            $ad_id = $get_account_campaign_ad_insights->data[0]->ad_id;

                            $this->FacebookInsights->UpdateAdAccountCampaignAdInsights($ad_account_id, $campaign_id, $adset_id, $get_account_campaign_ad_insights, $id);

                            $downloading_info['adaccounts'][$ad_account_id]['campaigns'][$campaign_id]['ads'][$ad_id]['insights'] = $get_account_campaign_ad_insights->data[0];
                        }
                    }
                }
            }
        }

        echo "<pre>"; print_r($downloading_info);

        //echo json_encode(array($downloading_info));

        exit;
    }
}

?>