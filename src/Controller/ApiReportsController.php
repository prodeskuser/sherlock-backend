<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ApiReportsController extends AppController{
    public function initialize()
    {
        parent::initialize();

        // Include the FlashComponent
        $this->loadComponent('Flash');

        // Auth component allow visitors to access add action to register and access logout action
        $this->Auth->allow(['results','get']);

        $this->loadModel('FacebookAdInsights');
    }


    /**
     * Central action to get reports from backend
     * @param $platform Facebook /
     * @param $account_id
     * @param $reportType by_date /
     */
    public function get ($platform, $account_id, $reportType)
    {
        //todo check that user have access to this account
        $from=isset ($this->request->query['from'])? $this->request->query['from']:date ('Y-m-d',microtime(true)-30*24*60*60);
        $to=isset ($this->request->query['to'])? $this->request->query['to']:date ('Y-m-d',microtime(true));
        $attr_window=isset ($this->request->query['attr_window'])?$this->request->query['attr_window'] : 'default';
        //todo take into account
        $filter_string="";

        $res = [];
        switch ($reportType)
        {
            case "by_date":
                $res = $this->FacebookAdInsights->byDate ($account_id,$from,$to,$filter_string,$attr_window);
                break;

        }

        //no valid response
        if (!$res || count($res)==0)
            $this->returnJsonResponse(false,$res);

        $this->returnJsonResponse(true,$res);
    }
}
