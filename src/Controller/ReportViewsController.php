<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class ReportViewsController extends AppController{

    public function initialize()
    {
        parent::initialize();

		// Auth component allow visitors to access add action to register and access logout action
        $this->Auth->allow(['index', 'addview']);
        $this->loadModel('ReportViews');
    }

    /*
     ** Responsible to return the user report views details
     */
    public function index(){
        return $this->ReportViews->getView(['user_id' => $this->Auth->user('id')]);
    }

    /*
     ** Save view
     */
    public function addview(){
        $insert_id = $this->ReportViews->saveView($this->request->data, $this->Auth->user('id'));
        if($insert_id){
            $viewdata = $this->ReportViews->getView(['user_id' => $this->Auth->user('id')]);
            $this->response->type('json');
            $this->response->body(json_encode(['viewdata'=>$viewdata]));
            return $this->response;
        }
    }
}
?>