<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ManualCustomReportsController extends AppController{
    public function initialize()
    {
        parent::initialize();

		// Auth component
        $this->Auth->allow(['index']);

        $this->loadModel('FacebookAdInsights');
        $this->loadModel('FacebookAdInsightsActions');
    }

	public function index($type=null, $owner_tag=null)
	{ 
        $this->autoRender = false; // avoid to render view
        $date="2018-01-18";
        $campaign_name_prefix = "";
        
        $where['date_start >'] = date("Y-m-d",strtotime($date));
        
        // Applying type filter
        if(isset($type) && (strtolower($type) == "prt" || strtolower($type) == "ras")){
            $campaign_name_prefix = $type."-";
        }

        // Applying owner filter
        if(isset($owner_tag) && (strtolower($owner_tag) == "prospecting" || strtolower($owner_tag) == "promo" || strtolower($owner_tag) == "rmk")){
            if($owner_tag == "Prospecting") {
               $campaign_name_prefix .= "acq"; 
            }
            else if($owner_tag == "Promo") {
               $campaign_name_prefix .= "pro"; 
            }
            else {
               $campaign_name_prefix .= "rmk"; 
            }
        }
        
        if($campaign_name_prefix != ''){
            $where['LOWER(campaign_name) LIKE'] = $campaign_name_prefix.'%';
        }

        $insightData =  $this->FacebookAdInsights->getInsightsData ($where);
        $insightDataActions =  $this->FacebookAdInsightsActions->getInsightsActionsData ($where);
       
        $insightDataActionsUpdated = array();
        foreach($insightDataActions as $val){
            $insightDataActionsUpdated[$val->id] = $val;
        }

        $return = array();
        foreach($insightData as $key=>$val){
            $return[$key]["impressions"] = $val["impressions"];
            $return[$key]["spend"] = $val["spend"];
            $return[$key]["inline_link_clicks"] = $val["inline_link_clicks"];
            $request_date = (array)$val["request_date"];
            $return[$key]["date_start"] = $request_date['date'];

            $str1 = $val["ad_id"].substr($request_date['date'], 0, -16).'1d_view';
            $id1 =  hash('sha256', $str1);

            $str2 = $val["ad_id"].substr($request_date['date'], 0, -16).'7d_click';
            $id2 =  hash('sha256', $str2);

            $str3 = $val["ad_id"].substr($request_date['date'], 0, -16).'28d_click';
            $id3 =  hash('sha256', $str3);

            $return[$key]["offsite_conversion.fb_pixel_purchase_1d_view"] = 0;
            $return[$key]["offsite_conversion.fb_pixel_purchase_7d_click"] = 0;
            $return[$key]["offsite_conversion.fb_pixel_purchase_28d_click"] = 0;
            $return[$key]["offsite_conversion.fb_pixel_purchase"] = 0;
            $return[$key]["offsite_conversion.fb_pixel_purchase_value"] = 0;
            $return[$key]["CTR"] = 0;
            $return[$key]["CPA"] = 0;
            $return[$key]["CPA_1v_7c"] = 0;
            
            $return[$key]["offsite_conversion.fb_pixel_purchase_1d_view"] += @$insightDataActionsUpdated[$id1]["fb_pixel_purchase"];
            $return[$key]["offsite_conversion.fb_pixel_purchase"] += @$insightDataActionsUpdated[$id1]["fb_pixel_purchase"];
            $return[$key]["offsite_conversion.fb_pixel_purchase_value"] += @$insightDataActionsUpdated[$id1]["fb_pixel_purchase_value"];

            $return[$key]["offsite_conversion.fb_pixel_purchase_7d_click"] = @$insightDataActionsUpdated[$id2]["fb_pixel_purchase"];
        
            $return[$key]["offsite_conversion.fb_pixel_purchase_28d_click"] = @$insightDataActionsUpdated[$id3]["fb_pixel_purchase"];
            $return[$key]["offsite_conversion.fb_pixel_purchase"] += @$insightDataActionsUpdated[$id3]["fb_pixel_purchase"];
            $return[$key]["offsite_conversion.fb_pixel_purchase_value"] += @$insightDataActionsUpdated[$id3]["fb_pixel_purchase_value"];

            $campaignNameArray = explode("-",$val["campaign_name"]);
            $owner_tag = $campaignNameArray[0];
            $type_tag = $campaignNameArray[1];
           
            if($owner_tag == "PRT" || $owner_tag == "RAS" || $owner_tag == ""){
                $return[$key]["Owner"] = $owner_tag;
            }
            if($type_tag == "RMK"){
               $return[$key]["Type"] = $type_tag;
            }
            if($type_tag == "PRO"){
               $return[$key]["Type"] = "Promo";
            }
            if($type_tag == "ACQ"){
               $return[$key]["Type"] = "Prospecting";
            }

            if($val['impressions'] > 0){
                $return[$key]["CTR"] = (($val['inline_link_clicks'])/($val['impressions']));
            }
            if($return[$key]["offsite_conversion.fb_pixel_purchase"] > 0){
                $return[$key]["CPA"] = (($val['spend'])/($return[$key]["offsite_conversion.fb_pixel_purchase"]));
            }
            if($return[$key]["offsite_conversion.fb_pixel_purchase_1d_view"] > 0){
                $return[$key]["CPA_1v_7c"] = $val['spend']/($return[$key]["offsite_conversion.fb_pixel_purchase_1d_view"]+$return[$key]["offsite_conversion.fb_pixel_purchase_7d_click"]);
            }
        }
        echo json_encode($return); die;
	}
}
?>