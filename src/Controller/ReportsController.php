<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class ReportsController extends AppController{

    public function initialize()
    {
        parent::initialize();

		// Include the FlashComponent
        $this->loadComponent('Flash');

		// Auth component allow visitors to access add action to register and access logout action
        $this->Auth->allow(['index', 'addview']);

        $this->loadModel('ColumnsConfig');
        $this->loadModel('FacebookAdAccounts');
        $this->loadModel('ReportViews');
    }

    /*
     ** Responsible to return the account details
     */
    public function index(){
    }
}
