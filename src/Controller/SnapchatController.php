<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

use App\Lib\Snapchat;

class SnapchatController extends AppController {
    public function initialize() {
        parent::initialize();
		$this->loadComponent('Flash');

		$this->Auth->allow(['index', 'login', 'getAuthenticatedUser', 'authLoginRedirect', 'getDownloadingInfo', 'getConfigurationData', 'getMeasurementData', 'UpdateCampaign', 'UpdateAdSquad', 'UpdateAd']);

        $this->loadModel('Users');
        $this->loadModel('SnapAccounts');
        $this->loadModel('SnapCampaigns');
        $this->loadModel('SnapAdSquads');
        $this->loadModel('SnapAds');

        $this->obj = new Snapchat;
    }

	public function index() {
        $snapchat_button = $this->obj->createButton();

        $this->set(compact('snapchat_button'));
	}

    public function authLoginRedirect() {
        $this->autoRender = false;
        return $this->redirect($this->obj->authRedirectURL($code, "authorization_code"));
    }

    public function login() {
        $this->autoRender = false;

        $code = $_GET['code'];

        $response = $this->obj->getAccessToken($code, "authorization_code");

        //echo "<pre>"; print_r($response);

        $access_token = $response['access_token'];
        $refresh_token = $response['refresh_token'];

        $this->Users->updateSnapchatInfo($access_token, $refresh_token, $this->Auth->user('id'));

        return $this->redirect('/snapchat/get-configuration-data/configuration');
    }

    public function getAuthenticatedUser() {
        $this->autoRender = false;

        $user_data = $this->Users->getUser(array('id' => $this->Auth->user('id')));

        $access_token = $user_data[0]->access_token;
        $refresh_token = $user_data[0]->refresh_token;

        $user_data_response = $this->obj->getAuthenticatedUser($access_token);

        if($user_data_response == 'invalid_token') {
            $token_response = $this->obj->getAccessToken($refresh_token, "refresh_token");

            $access_token = $token_response['access_token'];
            $refresh_token = $token_response['refresh_token'];

            $this->Users->updateSnapchatInfo($access_token, $refresh_token, $this->Auth->user('id'));

            $user_data_response = $this->obj->getAuthenticatedUser($access_token);
        }

        //echo "<pre>"; print_r($user_data_response);

        $this->response->type('json');

        if(!empty($user_data_response['me']))
            $this->response->body(json_encode(array('action' => 'success', 'access_token' => $access_token)));
        else
            $this->response->body(json_encode(array('action' => 'error')));

        return $this->response;
    }

    public function getDownloadingInfo($downloading_info_type = 'configuration') {
        $access_token_response = json_decode($this->getAuthenticatedUser());
        $access_token = $access_token_response->access_token;

        $organization_response = $this->obj->getOrganizations($access_token);

        $downloading_info = array();

        $ad_accounts = $this->obj->getAdAccounts($access_token, $organization_response);

        //echo "<pre>"; print_r($ad_accounts); die;

        if(!empty($ad_accounts)) {
            $last_updated = date('Y-m-d h:i:s');

            foreach($ad_accounts['adaccounts'] as $ad_account_key => $ad_account) {
                $ad_account_id = $ad_account['adaccount']['id'];

                if($downloading_info_type == 'configuration') {
                    $ad_account_data = $this->SnapAccounts->getAdAccount($this->Auth->user('id'), $organization_response, $ad_account_id);

                    //echo "<pre>"; print_r($ad_account_data);

                    if(!empty($ad_account_data))
                        $this->SnapAccounts->UpdateAdAccount($ad_account_data, $ad_account, $last_updated);
                    else
                        $this->SnapAccounts->addAdAccount($this->Auth->user('id'), $ad_account, $last_updated);
                }

                $downloading_info['adaccounts'][] = $ad_account['adaccount'];

                $ad_account_campaigns = $this->obj->getAdAccountCampaigns($access_token, $ad_account_id);

                //echo "<pre>"; print_r($ad_account_campaigns);

                foreach($ad_account_campaigns['campaigns'] as $ad_account_campaign_key => $ad_account_campaign) {
                    $campaign_id = $ad_account_campaign['campaign']['id'];

                    if($downloading_info_type == 'configuration') {
                        $ad_account_campaign_data = $this->SnapCampaigns->getAdAccountCampaign($this->Auth->user('id'), $campaign_id, $ad_account_id);

                        //echo "<pre>"; print_r($ad_account_campaign_data);

                        if(!empty($ad_account_campaign_data))
                            $this->SnapCampaigns->UpdateAdAccountCampaign($ad_account_campaign_data, $ad_account_campaign, $last_updated);
                        else
                            $this->SnapCampaigns->addAdAccountCampaign($this->Auth->user('id'), $ad_account_campaign, $last_updated);
                    }

                    $downloading_info['adaccounts'][$ad_account_id]['campaigns'][] = $ad_account_campaign['campaign'];

                    $ad_account_campaign_ad_squads = $this->obj->getAdAccountCampaignAdSquads($access_token, $campaign_id);

                    //echo "<pre>"; print_r($ad_account_campaign_ad_squads);

                    foreach($ad_account_campaign_ad_squads['adsquads'] as $ad_account_campaign_ad_squad_key => $ad_account_campaign_ad_squad) {
                        $ad_squad_id = $ad_account_campaign_ad_squad['adsquad']['id'];

                        if($downloading_info_type == 'configuration') {
                            $ad_account_campaign_ad_squad_data = $this->SnapAdSquads->getAdAccountCampaignAdSquads($this->Auth->user('id'), $campaign_id, $ad_account_id, $ad_squad_id);

                            //echo "<pre>"; print_r($ad_account_campaign_ad_squad_data);

                            if(!empty($ad_account_campaign_ad_squad_data))
                                $this->SnapAdSquads->UpdateAdAccountCampaignAdSquad($ad_account_campaign_ad_squad_data, $ad_account_campaign_ad_squad, $last_updated);
                            else
                                $this->SnapAdSquads->addAdAccountCampaignAdSquad($this->Auth->user('id'), $ad_account_id, $ad_account_campaign_ad_squad, $last_updated);
                        }

                        $downloading_info['adaccounts'][$ad_account_id]['campaigns'][$campaign_id]['adsquads'][] = $ad_account_campaign_ad_squad['adsquad'];

                        $ad_squad_ads = $this->obj->getAdSquadAds($access_token, $ad_squad_id);

                        //echo "<pre>"; print_r($ad_squad_ads);

                        foreach($ad_squad_ads['ads'] as $ad_squad_ad_key => $ad_squad_ad) {
                            $ad_id = $ad_squad_ad['ad']['id'];

                            $ad_account_campaign_ad_squad_ad_data = $this->SnapAds->getAdAccountCampaignAdSquadsAd($this->Auth->user('id'), $ad_account_id,$campaign_id, $ad_squad_id, $ad_id);

                            //echo "<pre>"; print_r($ad_account_campaign_ad_squad_ad_data);

                            if($downloading_info_type == 'configuration') {
                                if(!empty($ad_account_campaign_ad_squad_ad_data))
                                    $this->SnapAds->UpdateAdAccountCampaignAdSquadAd($ad_account_campaign_ad_squad_ad_data, $ad_squad_ad, $ad_account_id,$campaign_id, $ad_squad_id, $last_updated);
                                else
                                    $this->SnapAds->addAdAccountCampaignAdSquadAd($this->Auth->user('id'), $ad_account, $ad_account_campaign, $ad_account_campaign_ad_squad, $ad_squad_ad, $last_updated);
                            }

                            $downloading_info['adaccounts'][$ad_account_id]['campaigns'][$campaign_id]['adsquads'][$ad_squad_id]['ads'][] = $ad_squad_ad['ad'];

                            if($downloading_info_type == 'measurement') {
                                $ad_stats = $this->obj->getAdStatus($access_token, $ad_id);

                                //echo "<pre>"; print_r($ad_stats);

                                foreach($ad_stats['total_stats'] as $ad_stat) {
                                    $this->SnapAds->UpdateAdAccountCampaignAdSquadAdStats($ad_account_campaign_ad_squad_ad_data, $ad_stat, $last_updated);

                                    $downloading_info['adaccounts'][$ad_account_id]['campaigns'][$campaign_id]['adsquads'][$ad_squad_id]['ads'][$ad_id]['stats'][] = $ad_stat['total_stat'];
                                }
                            }
                        }
                    }
                }
            }

            //Set delete status to 1 of all other campaigns/ad sets/ads whose last_updated time is less than $last_updated variable i.e. those are deleted from Snapchat Account
            $this->SnapAccounts->setAdAccountDeleteStatus($this->Auth->user('id'), $last_updated);
            $this->SnapCampaigns->setAdAccountCampaignDeleteStatus($this->Auth->user('id'), $last_updated);
            $this->SnapAdSquads->setAdAccountCampaignAdSquadDeleteStatus($this->Auth->user('id'), $last_updated);
            $this->SnapAds->setAdAccountCampaignAdSquadAdDeleteStatus($this->Auth->user('id'), $last_updated);
        }

        //echo "<pre>"; print_r($downloading_info);

        echo json_encode(array($downloading_info));

        exit;
    }

    public function getConfigurationData() {
        $this->autoRender = false;

        $configuration_data = $this->getDownloadingInfo('configuration');

        //echo "<pre>"; print_r($configuration_data);

        $this->response->type('json');
        $this->response->body(json_encode($configuration_data));
        return $this->response;
    }

    public function getMeasurementData() {
        $this->autoRender = false;

        $measurement_data = $this->getDownloadingInfo('measurement');

        //echo "<pre>"; print_r($measurement_data);

        $this->response->type('json');
        $this->response->body(json_encode($measurement_data));
        return $this->response;
    }

    public function UpdateCampaign() {
        $this->autoRender = false;

        $ad_account_id = 'c3db59e9-7c91-41fb-887b-6f068ba8af71';
        $campaign_id = '6b27807c-5a1c-4977-8c78-b5f88646ab35';

        if($this->request->query('status') == '')
            $status = 'ACTIVE';
        else if($this->request->query('status') == 1)
            $status = 'ACTIVE';
        else if($this->request->query('status') == 0)
            $status = 'PAUSED';

        if($this->request->query('daily_budget_micro') == '')
            $daily_budget_micro = 50000000;
        else if($this->request->query('daily_budget_micro') != '')
            $daily_budget_micro = $this->request->query('daily_budget_micro');

        $params = array("campaigns" => array(array("id" => $campaign_id, "name" => 'campaign updated', "ad_account_id" => $ad_account_id, "daily_budget_micro" => $daily_budget_micro, "start_time" => "2018-04-16T06:48:16", "status" => $status)));

        $access_token_response = json_decode($this->getAuthenticatedUser());
        $access_token = $access_token_response->access_token;

        $response = $this->obj->UpdateCampaign($access_token, $ad_account_id, $params);

        //echo "<pre>"; print_r($response);

        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function UpdateAdSquad() {
        $this->autoRender = false;

        $campaign_id = "6b27807c-5a1c-4977-8c78-b5f88646ab35";
        $ad_squad_id = "3f4e5a4c-49eb-483a-9c6f-2fd51fa39558";

        if($this->request->query('status') == '')
            $status = 'ACTIVE';
        else if($this->request->query('status') == 1)
            $status = 'ACTIVE';
        else if($this->request->query('status') == 0)
            $status = 'PAUSED';

        if($this->request->query('bid_micro') == '')
            $bid_micro = 10000;
        else if($this->request->query('bid_micro') != '')
            $bid_micro = $this->request->query('bid_micro');

        if($this->request->query('daily_budget_micro') == '')
            $daily_budget_micro = 50000000;
        else if($this->request->query('daily_budget_micro') != '')
            $daily_budget_micro = $this->request->query('daily_budget_micro');

        $params = array("adsquads" => array(array("id" => $ad_squad_id, "campaign_id" => $campaign_id, "name" => "ad squad updated", "type" => "SNAP_ADS", "placement" => "SNAP_ADS", "optimization_goal" => "IMPRESSIONS", "bid_micro" => $bid_micro, "daily_budget_micro" => $daily_budget_micro, "status" => $status, "targeting" => (array("regulated_content" => false, "geos" => array(array("country_code" => "in", "region_id" => array("10027"))))))));

        $access_token_response = json_decode($this->getAuthenticatedUser());
        $access_token = $access_token_response->access_token;

        $response = $this->obj->UpdateAdSquad($access_token, $campaign_id, $params);

        //echo "<pre>"; print_r($response); die;

        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function UpdateAd() {
        $this->autoRender = false;

        $ad_id = "ff546c9c-e0da-494c-8e45-031b05cdd922";
        $ad_squad_id = "3f4e5a4c-49eb-483a-9c6f-2fd51fa39558";
        $creative_id = "d1e5cc91-2217-4981-96d5-84d416e56ca7";

        if($this->request->query('status') == '')
            $status = 'ACTIVE';
        else if($this->request->query('status') == 1)
            $status = 'ACTIVE';
        else if($this->request->query('status') == 0)
            $status = 'PAUSED';

        $params = array("ads" => array(array("id" => $ad_id, "ad_squad_id" => $ad_squad_id, "creative_id" => $creative_id, "name" => "test", "type" => "SNAP_AD", "status" => $status)));

        $access_token_response = json_decode($this->getAuthenticatedUser());
        $access_token = $access_token_response->access_token;

        $response = $this->obj->UpdateAd($access_token, $ad_squad_id, $params);

        $this->response->type('json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
}

?>