<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class UserColumnsController extends AppController {
    public function initialize()
    {
        parent::initialize();
		$this->loadComponent('Flash'); // Include the FlashComponent
		// Auth component allow visitors to access add action to register  and access logout action 
		$this->Auth->allow(['index', 'saveColumn', 'updateColumn', 'columnsConfig', 'getUserColumnsConfigData', 'removeColumn', 'getReportConfigData']);
        $this->loadModel('ColumnsConfig');
        $this->loadModel('ReportViews');
        $this->loadModel('FacebookAdAccounts');

        // Include the FlashComponent
        $this->loadComponent('Flash');
    }

	public function index(){}
	/*
     ** Responsible to insert new row in columns config table
     */
    public function saveColumn() {
        $this->autoRender = false;
        $data = $this->request->data['data'];
        $data['user_id'] =  $this->Auth->user('id');
        $result= $this->ColumnsConfig->saveColumn($data);

        $this->response->type('json');
        $this->response->body((isset($result) ? json_encode(array('msg' => 'success')) : json_encode(array('msg' => 'error'))));
        return $this->response;
    }

    /*
     ** Reponsible to updated column config row
     */
    public function updateColumn() {
        $this->autoRender = false;
        $result= $this->ColumnsConfig->updateColumns($this->request->data['data']['oldData'], $this->request->data['data']['newData']);
        $this->response->type('json');
        $this->response->body((isset($result) ? json_encode(array('msg' => 'success')) : json_encode(array('msg' => 'error'))));
        return $this->response;
    }

    /*
     ** End point to return columns_config records (with filter, sort)
    */
    public function getUserColumnsConfigData() {
        $this->autoRender = false;
        $orderby = (isset($this->request->data['orderby']) ? $this->request->data['orderby'] : null);
        $query = $this->ColumnsConfig->getColumns(['user_id' => $this->Auth->user('id')], $orderby);

        $this->response->type('json');
        $this->response->body(json_encode(array("totalCount" => $query->count(), 'result' => $query)));
        return $this->response;
    }

    /*
     ** Reponsible to Delete column config
    */
    public function removeColumn() {
        $this->autoRender = false;
        $result= $this->ColumnsConfig->removeColumn($this->request->data['data']);

        $this->response->type('json');
        $this->response->body((isset($result) ? json_encode(array('msg' => 'success')) : json_encode(array('msg' => 'error'))));
        return $this->response;
    }
    
    public function getReportConfigData(){
        $this->autoRender = false;
        
        $data['columns_config_data'] = $this->ColumnsConfig->getColumns(['user_id' => $this->Auth->user('id')]);
        $data['user_views'] = $this->ReportViews->getUserViews(['user_id' => $this->Auth->user('id')]);
        $data['user_views']["default"] = json_encode(Configure::read('DEFAULT_COLUMNS'));

        $rawArray = $this->FacebookAdAccounts->getUserAccountData($this->Auth->user('id'));
        $filterdata = [];
        $account_id =[];
        foreach ($rawArray->toArray() as $key =>$value) {

            if (in_array($value['account_id'], $account_id))
            {
                $filterdata[$value['account_id']]['user_views'][] = $value['ReportViews']['view_name'];
            }else{
               $account_id[] = $value['account_id'];
               $filterdata[$value['account_id']]['account_id']    = $value['account_id'];
               $filterdata[$value['account_id']]['account_name']  = $value['name'];
               $filterdata[$value['account_id']]['attribution_window'] = 
               explode(",",str_replace('"', '', str_replace( array('[',']') , ''  , $value['attribution_window'] )));
               $filterdata[$value['account_id']]['user_views'][]  = "default";
               if($value['ReportViews']['view_name']) {
                   $filterdata[$value['account_id']]['user_views'][]  = $value['ReportViews']['view_name'];
               }
            }
        }
        $data['account_data'] = $filterdata;
        $this->response->type('json');
        $this->response->body(json_encode($data));
        return $this->response;
    }
}