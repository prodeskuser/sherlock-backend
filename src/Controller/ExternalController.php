<?php
/**
 * Created by PhpStorm.
 * User: Jarome
 * Date: 10/14/2018
 * Time: 1:21 PM
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\NumberHelper;
use App\View\Helper\DevExpressDataGridHelper;
use Cake\I18n\Time;
use Cake\I18n\Date;

class ExternalController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['fiverr']);
        $this->loadModel('FacebookAdInsights');
    }

    public function fiverr ()
    {
        $map =[
            'external1'=>'regs', 'external2'=>'purchases'
        ];

        $regs_col = 'external1';
        $ftds_col = 'external2';

        $this->log ('new External request','debug');
        if (!isset($this->request->data['data'])) exit;
        $data = $this->request->data['data'];

        $this->log ('Raw MSG:','debug');
        $this->log (print_r ($data,true),'debug');

        $matches=[];
        $data = trim(preg_replace('/\s+/', ' ', $data));
        preg_match('~(\d\d\d\d\-\d\d\-\d\d\s.*\s\d+\s\d+)~', $data, $matches, PREG_OFFSET_CAPTURE);

        $clean_msg ="";
        if (isset($matches[0][0]))
            $clean_msg = $matches[0][0];

        if (empty($clean_msg))
        {
            $this->log ('Invalid msg - clean is empty, Exiting','debug');
            exit;
        }


//     $msg_one_line="2018-03-03 pareto_ww_cate-shortvideoads_iphone_in_mob_ai geo:ww^lng:all^pct:in^gol:purchase^dvc:iphone^tgt:ll_prom_video_buyers_1p^gnd:m^age:25-55 23842753420400159 7 0 0";
        $msg_one_line=$clean_msg;
        $transactions = preg_split('/(?=\d\d\d\d\-\d\d\-\d\d)/', $msg_one_line);

//        if there are less than 2 rows (we have more) then invalid
        if (count($transactions) < 2)
        {
            $this->log ('Invalid msg, Exiting','debug');
            exit;
        }
//
        $this->log (print_r ($transactions,true),'debug');

        $account_info=[];
        $account_info['account_id']=10156289365914286;

        $by_ad_id = [];
        //aggreate report by ad_id, fiver report not unique for ad_id
        foreach ($transactions as $row)
        {
            if (empty($row) || strlen($row)==0)
                continue;

            $elms = explode(' ',$row);

            $date =$elms[0];
            $ad_id =$elms[3];
            $regs =$elms[4];
            $ftbs =$elms[5];

            $this->log ($row,'debug');
            $this->log (print_r ($elms,true),'debug');

            if (!isset ($by_ad_id[$ad_id]))
            {
                $by_ad_id[$ad_id]=[
                    'request_date'=>new Time($date),
                    'ad_id'=>$ad_id,
                    $regs_col=>$regs,
                    $ftds_col=>$ftbs];
                continue;
            }

            $by_ad_id[$ad_id][$regs_col]+=$regs;
            $by_ad_id[$ad_id][$ftds_col]+=$ftbs;

        }

        $this->log (print_r ($by_ad_id,true),'debug');


        $new_rows = [];

        foreach ($by_ad_id as $row)
        {
            $new_rows[]=$row;
        }

        //final result
        $this->log (print_r ($new_rows,true),'debug');
        $this->FacebookAdInsights->saveInsights ($new_rows);


        exit;
    }

}