<?php 
return [
    'DEFAULT_COLUMNS' => [
            'columns' => [
                [
                    'dataField' => 'request_date',
                    'dataType' => 'date',
                    'formate' => 'dd-MM-yyyy',
                    'visible' => true,
                    'visibleIndex' => 1
                ],[
                    'dataField' => 'clicks',
                    'dataType' => 'number',
                    'visible' => true,
                    'visibleIndex' => 2
                ],[
                    'dataField' => 'impressions',
                    'dataType' => 'number',
                    'visible' => true,
                    'visibleIndex' => 3
                ],[
                    'dataField' => 'spend',
                    'dataType' => 'number',
                    'visible' => true,
                    'visibleIndex' => 4
                ]
            ]
        ]
    ];